
include(FetchContent)

if (WIN32)
    # Bad, bad, bad Windows....

    FetchContent_Declare(
            sdl2scripts
            GIT_REPOSITORY "https://github.com/tcbrindle/sdl2-cmake-scripts"
            GIT_TAG "e037fb54f32973343fada6a051d3a3f8adf3a4a0"
    )
    FetchContent_GetProperties(sdl2scripts)
    if (NOT sdl2scripts_POPULATED)
        FetchContent_Populate(sdl2scripts)
        #message(STATUS "sdl2scripts_SOURCE_DIR: ${sdl2scripts_SOURCE_DIR}")
        set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${sdl2scripts_SOURCE_DIR}")
    endif ()

    set(SDL2_PATH $ENV{SDL2_DIR})
    set(SDL2_IMAGE_PATH $ENV{SDL2_IMAGE_DIR})
    set(SDL2_TTF_PATH $ENV{SDL2_TTF_DIR})
    set(SDL2_MIXER_PATH $ENV{SDL2_MIXER_DIR})
endif ()

set(BUILD_STATIC_LIBS ON CACHE BOOL "" FORCE)

set(_LIBRARIES)
set(_INCLUDE_DIRS)

FetchContent_Declare(
        glm
        GIT_REPOSITORY "https://github.com/g-truc/glm"
        GIT_TAG "6ad79aae3eb5bf809c30bf1168171e9e55857e45"
)
FetchContent_MakeAvailable(glm)
list(APPEND _LIBRARIES "glm::glm_static")

if (WIN32)
    set(GLEW_PATH "$ENV{GLEW_DIR}")
    set(GLEW_INCLUDE_DIRS "${GLEW_PATH}/include")
    set(GLEW_LIBRARIES "${GLEW_PATH}/lib")
    if (GLEW_DEBUG_MODE)
        set(GLEW_LIBRARY_BUILD_TYPE "Debug")
    else ()
        set(GLEW_LIBRARY_BUILD_TYPE "Release")
    endif ()
    if (CMAKE_SIZEOF_VOID_P EQUAL 8)
        set(GLEW_LIBRARY_ARCH "x64")
    else ()
        set(GLEW_LIBRARY_ARCH "Win32")
    endif ()
    set(GLEW_STATIC_LIBRARIES "${GLEW_LIBRARIES}/${GLEW_LIBRARY_BUILD_TYPE}/${GLEW_LIBRARY_ARCH}/glew32s.lib")
    set(GLEW_SHARED_LIBRARIES "${GLEW_LIBRARIES}/${GLEW_LIBRARY_BUILD_TYPE}/${GLEW_LIBRARY_ARCH}/glew32.lib")
    set(GLEW_DLL_LIBRARIES "${GLEW_PATH}/bin/${GLEW_LIBRARY_BUILD_TYPE}/${GLEW_LIBRARY_ARCH}/glew32.dll")
else ()
    # TODO: test other OS
    find_package(GLEW REQUIRED)
endif ()
list(APPEND _LIBRARIES "${GLEW_SHARED_LIBRARIES}")
list(APPEND _INCLUDE_DIRS "${GLEW_INCLUDE_DIRS}")
set(GLEW_STATIC_LIBRARIES ${GLEW_STATIC_LIBRARIES} PARENT_SCOPE)
set(GLEW_SHARED_LIBRARIES ${GLEW_SHARED_LIBRARIES} PARENT_SCOPE)
set(GLEW_DLL_LIBRARIES ${GLEW_DLL_LIBRARIES} PARENT_SCOPE)

find_package(OpenGL REQUIRED)
list(APPEND _LIBRARIES "${OPENGL_LIBRARIES}")
list(APPEND _INCLUDE_DIRS "${OPENGL_INCLUDE_DIRS}")

find_package(SDL2 REQUIRED)
list(APPEND _LIBRARIES "${SDL2_LIBRARY}")
list(APPEND _INCLUDE_DIRS "${SDL2_INCLUDE_DIR}")

find_package(SDL2_image REQUIRED)
list(APPEND _LIBRARIES "${SDL2_IMAGE_LIBRARY}")
list(APPEND _INCLUDE_DIRS "${SDL2_IMAGE_INCLUDE_DIR}")

find_package(SDL2_ttf REQUIRED)
list(APPEND _LIBRARIES "${SDL2_TTF_LIBRARY}")
list(APPEND _INCLUDE_DIRS "${SDL2_TTF_INCLUDE_DIR}")

find_package(SDL2_mixer REQUIRED)
list(APPEND _LIBRARIES "${SDL2_MIXER_LIBRARY}")
list(APPEND _INCLUDE_DIRS "${SDL2_MIXER_INCLUDE_DIR}")

set_property(GLOBAL PROPERTY NAGE_DEPENDENCIES_LIBRARIES "${_LIBRARIES}")
set_property(GLOBAL PROPERTY NAGE_DEPENDENCIES_INCLUDES "${_INCLUDE_DIRS}")

function(_link_deps TARGET_NAME)
    get_property(_VAR GLOBAL PROPERTY NAGE_DEPENDENCIES_LIBRARIES)
    #message(STATUS "NAGE_DEPENDENCIES_LIBRARIES: ${_VAR}")
    target_link_libraries(${TARGET_NAME} ${_VAR})
    get_property(_VAR GLOBAL PROPERTY NAGE_DEPENDENCIES_INCLUDES)
    #message(STATUS "NAGE_DEPENDENCIES_INCLUDES: ${_VAR}")
    target_include_directories(${TARGET_NAME} PUBLIC ${_VAR})
endfunction()

function(copy_dependencies TARGET_NAME)
    get_property(_TARGET_LOCATION TARGET ${TARGET_NAME} PROPERTY RUNTIME_OUTPUT_DIRECTORY)

    if (WIN32)
        function(_copy_sdl_dll LIBDIR)
            #message(STATUS "LIBDIR: ${LIBDIR}")
            #message(STATUS "ARGN: ${ARGN}")
            set(_LIB)
            foreach (_LIB_IT ${LIBDIR})
                set(_LIB ${_LIB_IT})
                if (_LIB)
                    break()
                endif ()
            endforeach ()
            if (NOT _LIB)
                return()
            endif ()
            get_filename_component(_LIB_DIR ${_LIB} DIRECTORY)
            foreach (_LIBNAME IN LISTS ARGN)
                set(_LIB "${_LIB_DIR}/${_LIBNAME}.dll")
                if (NOT EXISTS ${_LIB})
                    continue()
                endif ()
                post_build_copy_files(${TARGET_NAME} "${_TARGET_LOCATION}" "${_LIB}")
            endforeach ()
        endfunction()

        _copy_sdl_dll(${SDL2_LIBRARY} "SDL2")

        set(_IMAGE_LIBS "SDL2_image" "zlib1")
        if (SUPPORT_IMAGE_JPEG)
            list(APPEND _IMAGE_LIBS "libjpeg-9")
        endif ()
        if (SUPPORT_IMAGE_PNG)
            list(APPEND _IMAGE_LIBS "libpng16-16")
        endif ()
        if (SUPPORT_IMAGE_TIFF)
            list(APPEND _IMAGE_LIBS "libtiff-5")
        endif ()
        if (SUPPORT_IMAGE_WEBP)
            list(APPEND _IMAGE_LIBS "libwebp-7")
        endif ()
        _copy_sdl_dll(${SDL2_IMAGE_LIBRARY} ${_IMAGE_LIBS})

        _copy_sdl_dll(${SDL2_TTF_LIBRARY} "SDL2_ttf")

        set(_MIXER_LIBS "SDL2_mixer")
        if (SUPPORT_MIXER_FLAC)
            list(APPEND _MIXER_LIBS "libFLAC-8")
        endif ()
        if (SUPPORT_MIXER_MODPLUG)
            list(APPEND _MIXER_LIBS "libmodplug-1")
        endif ()
        if (SUPPORT_MIXER_MPG123)
            list(APPEND _MIXER_LIBS "libmpg123")
        endif ()
        if (SUPPORT_MIXER_OGG)
            list(APPEND _MIXER_LIBS "libogg-0")
        endif ()
        if (SUPPORT_MIXER_OPUS)
            list(APPEND _MIXER_LIBS "libopus-0" "libopusfile-0")
        endif ()
        if (SUPPORT_MIXER_VORBIS)
            list(APPEND _MIXER_LIBS "libvorbis-0" "libvorbisfile-3")
        endif ()
        _copy_sdl_dll(${SDL2_MIXER_LIBRARY} ${_MIXER_LIBS})

        post_build_copy_files(${TARGET_NAME} "${_TARGET_LOCATION}" "${GLEW_DLL_LIBRARIES}")
    endif ()

    if (SUPPORT_IMAGE_JPEG)
        add_definitions(-D_NAGE_CONFIG_SDL_IMAGE_SUPPORT_JPEG)
    endif ()
    if (SUPPORT_IMAGE_PNG)
        add_definitions(-D_NAGE_CONFIG_SDL_IMAGE_SUPPORT_PNG)
    endif ()
    if (SUPPORT_IMAGE_TIFF)
        add_definitions(-D_NAGE_CONFIG_SDL_IMAGE_SUPPORT_TIFF)
    endif ()
    if (SUPPORT_IMAGE_WEBP)
        add_definitions(-D_NAGE_CONFIG_SDL_IMAGE_SUPPORT_WEBP)
    endif ()

    if (SUPPORT_MIXER_FLAC)
        add_definitions(-D_NAGE_CONFIG_SDL_MIXER_SUPPORT_FLAC)
    endif ()
    if (SUPPORT_MIXER_MODPLUG)
        add_definitions(-D_NAGE_CONFIG_SDL_MIXER_SUPPORT_MODPLUG)
    endif ()
    if (SUPPORT_MIXER_MPG123)
        add_definitions(-D_NAGE_CONFIG_SDL_MIXER_SUPPORT_MPG123)
    endif ()
    if (SUPPORT_MIXER_OGG)
        add_definitions(-D_NAGE_CONFIG_SDL_MIXER_SUPPORT_OGG)
    endif ()
    if (SUPPORT_MIXER_OPUS)
        add_definitions(-D_NAGE_CONFIG_SDL_MIXER_SUPPORT_OPUS)
    endif ()
    if (SUPPORT_MIXER_VORBIS)
        add_definitions(-D_NAGE_CONFIG_SDL_MIXER_SUPPORT_VORBIS)
    endif ()
endfunction()
