
function(read_version_from_file FILE_PATH VERSION_PREFIX)
    file(READ "${FILE_PATH}" _VERSION_FILE)
    string(REGEX MATCH "#define[ ]+${VERSION_PREFIX}_VERSION_MAJOR[ ]+([0-9]+)" _ ${_VERSION_FILE})
    set("${VERSION_PREFIX}_VERSION_MAJOR" "${CMAKE_MATCH_1}" PARENT_SCOPE)
    set(_VERSION_MAJOR "${CMAKE_MATCH_1}")
    string(REGEX MATCH "#define[ ]+${VERSION_PREFIX}_VERSION_MINOR[ ]+([0-9]+)" _ ${_VERSION_FILE})
    set("${VERSION_PREFIX}_VERSION_MINOR" "${CMAKE_MATCH_1}" PARENT_SCOPE)
    set(_VERSION_MINOR "${CMAKE_MATCH_1}")
    string(REGEX MATCH "#define[ ]+${VERSION_PREFIX}_VERSION_PATCH[ ]+([0-9]+)" _ ${_VERSION_FILE})
    set(_VERSION_PATCH "${CMAKE_MATCH_1}")
    set("${VERSION_PREFIX}_VERSION_PATCH" "${CMAKE_MATCH_1}" PARENT_SCOPE)
    string(REGEX MATCH "#define[ ]+${VERSION_PREFIX}_VERSION_REVISION[ ]+([0-9]+)" _ ${_VERSION_FILE})
    set("${VERSION_PREFIX}_VERSION_REVISION" "${CMAKE_MATCH_1}" PARENT_SCOPE)
    set(_VERSION_REVISION "${CMAKE_MATCH_1}")
    set("${VERSION_PREFIX}_VERSION"
            "${_VERSION_MAJOR}.${_VERSION_MINOR}.${_VERSION_PATCH}.${_VERSION_REVISION}"
            PARENT_SCOPE)
endfunction()

function(post_build_copy_files TARGET_NAME DST_DIR)
    file(MAKE_DIRECTORY ${DST_DIR})
    foreach (_FILENAME IN LISTS ARGN)
        add_custom_command(TARGET ${TARGET_NAME} POST_BUILD
                COMMAND ${CMAKE_COMMAND} -E copy
                "${_FILENAME}"
                "${DST_DIR}"
                COMMENT "Copy ${_FILENAME} to ${DST_DIR}"
                )
    endforeach ()
endfunction()

function(__copy_assets_impl__ DST_OUT_VAR SRC_OUT_VAR OUT_DIR IN_DIR)
    set(_RES_OUT_LIST)
    set(_RES_IN_LIST)
    file(MAKE_DIRECTORY ${OUT_DIR})
    foreach (_PATTERN IN LISTS ARGN)
        file(GLOB_RECURSE _LIST0 "${IN_DIR}/${_PATTERN}")
        foreach (_ITEM ${_LIST0})
            get_filename_component(_NAME ${_ITEM} NAME)
            set(_DEPEND "${OUT_DIR}/${_NAME}")
            add_custom_command(
                    OUTPUT "${_DEPEND}"
                    COMMAND ${CMAKE_COMMAND} -E copy
                    "${_ITEM}"
                    "${OUT_DIR}"
                    MAIN_DEPENDENCY "${_ITEM}")
            list(APPEND _RES_OUT_LIST "${_DEPEND}")
            list(APPEND _RES_IN_LIST "${_ITEM}")
        endforeach ()
    endforeach ()
    set(${DST_OUT_VAR} ${${DST_OUT_VAR}} ${_RES_OUT_LIST} PARENT_SCOPE)
    set(${SRC_OUT_VAR} ${${SRC_OUT_VAR}} ${_RES_IN_LIST} PARENT_SCOPE)
endfunction()

function(__copy_assets_internal__ DST_OUT_VAR SRC_OUT_VAR)
    list(LENGTH ARGN _ARGN_LEN)
    if (${_ARGN_LEN} GREATER 2)
        list(GET ARGN 0 ASSET_OUT)
        list(GET ARGN 1 ASSET_IN)
        list(SUBLIST ARGN 2 -1 ASSET_PATTERNS)
        message(STATUS "debug ASSET_OUT: ${ASSET_OUT}")
        message(STATUS "debug ASSET_IN: ${ASSET_IN}")
        message(STATUS "debug ASSET_PATTERNS: ${ASSET_PATTERNS}")
        __copy_assets_impl__(_L1 _L2
                "${_OUT_DIR}/${ASSET_OUT}"
                "${ASSET_IN}"
                "${ASSET_PATTERNS}")
        set(${DST_OUT_VAR} ${${DST_OUT_VAR}} ${_L1} PARENT_SCOPE)
        set(${SRC_OUT_VAR} ${${SRC_OUT_VAR}} ${_L2} PARENT_SCOPE)
    endif ()
endfunction()

function(copy_assets TARGET_NAME)
    get_property(_TARGET_LOCATION TARGET ${TARGET_NAME} PROPERTY RUNTIME_OUTPUT_DIRECTORY)
    set(_OUT_DIR "${_TARGET_LOCATION}/assets")
    file(MAKE_DIRECTORY "${_OUT_DIR}")

    set(_DEPENDS)
    set(_SRC)

    set(_CURRENT_ARGS)
    foreach (_ARG IN LISTS ARGN)
        if ("${_ARG}" STREQUAL "ASSET")
            __copy_assets_internal__(_DEPENDS _SRC ${_CURRENT_ARGS})
            set(_CURRENT_ARGS)
        else ()
            list(APPEND _CURRENT_ARGS "${_ARG}")
        endif ()
    endforeach ()
    __copy_assets_internal__(_DEPENDS _SRC "${_CURRENT_ARGS}")

    add_custom_target("${TARGET_NAME}.copy_assets"
            DEPENDS ${_DEPENDS}
            SOURCES ${_SRC})
    add_dependencies(${TARGET_NAME} "${TARGET_NAME}.copy_assets")
endfunction()
