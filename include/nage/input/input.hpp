//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_INPUT_HPP
#define NAGE_INPUT_HPP

#include <mutex>
#include <map>
#include <list>
#include <set>
#include <SDL.h>

namespace nage::app { class Application; }

namespace nage::input {

	using Scancode = SDL_Scancode;

	enum class GamepadButton : uint16_t {
		None = 0,
		A = 0x1 << 0,
		B = 0x1 << 1,
		X = 0x1 << 2,
		Y = 0x1 << 3,
		BACK = 0x1 << 4,
		GUIDE = 0x1 << 5,
		START = 0x1 << 6,
		LEFT_STICK = 0x1 << 7,
		RIGHT_STICK = 0x1 << 8,
		LEFT_SHOULDER = 0x1 << 9,
		RIGHT_SHOULDER = 0x1 << 10,
		DPAD_UP = 0x1 << 11,
		DPAD_DOWN = 0x1 << 12,
		DPAD_LEFT = 0x1 << 13,
		DPAD_RIGHT = 0x1 << 14,

		ALL = A | B | X | Y |
			  BACK | GUIDE | START |
			  LEFT_STICK | RIGHT_STICK |
			  LEFT_SHOULDER | RIGHT_SHOULDER |
			  DPAD_UP | DPAD_DOWN | DPAD_LEFT | DPAD_RIGHT
	};

	inline GamepadButton operator|(GamepadButton x, GamepadButton y) {
		return GamepadButton(uint16_t(x) | uint16_t(y));
	}

	inline GamepadButton &operator|=(GamepadButton &x, GamepadButton y) {
		return (x = x | y);
	}

	inline GamepadButton operator&(GamepadButton x, GamepadButton y) {
		return GamepadButton(uint16_t(x) & uint16_t(y));
	}

	inline GamepadButton &operator&=(GamepadButton &x, GamepadButton y) {
		return (x = x & y);
	}

	inline GamepadButton operator~(GamepadButton x) {
		return GamepadButton(~uint16_t(x)) & GamepadButton::ALL;
	}

	enum class Axis : int8_t {
		Invalid = -1,
		LeftX,
		LeftY,
		RightX,
		RightY,
		TriggerLeft,
		TriggerRight,
	};

	class AxisBinding {
		friend class Input;

		Axis m_axis;
		std::map<Scancode, float> m_keys;

		explicit AxisBinding(Axis axis);

	public:
		AxisBinding();

		~AxisBinding() = default;

		AxisBinding &addKey(Scancode scancode, float value);

		AxisBinding &removeKey(Scancode scancode);

		[[nodiscard]] Axis getAxis() const;

	private:
		void emit();
	};

	class ButtonBinding {
		friend class Input;

		GamepadButton m_button;
		std::set<Scancode> m_keys;

		explicit ButtonBinding(GamepadButton button);

	public:
		ButtonBinding();

		~ButtonBinding() = default;

		ButtonBinding &addKey(Scancode scancode);

		ButtonBinding &removeKey(Scancode scancode);

		[[nodiscard]] GamepadButton getButton() const;

	private:
		void emit();
	};

	struct IInputEventListener {
		virtual ~IInputEventListener() = default;

		virtual void onGamepadAttached() {};

		virtual void onGamepadDetached() {}
	};

	class Input final {
		friend class ::nage::app::Application;

		friend class AxisBinding;

		friend class ButtonBinding;

		Input() = default;

		static Input &get();

		struct Axes {
			union {
				float array[6]{0};
				struct {
					float lx;
					float ly;
					float rx;
					float ry;
					float lt;
					float rt;
				};
			};
		};

		struct GamepadButtons {
			GamepadButton mask{GamepadButton::None};
		};

		struct Gamepad {
			std::string name;
			int index{-1};
			Axes axes;
			GamepadButtons buttons;
		};
		std::map<SDL_GameController *, Gamepad> m_g;

		Gamepad m_virtualGamepad{"Keyboard"};

		const Uint8 *m_keyboard{nullptr};

		std::mutex m_mtx;
		using lock_t = std::lock_guard<std::mutex>;
		std::list<IInputEventListener *> m_inputEventListeners;

		std::map<Axis, AxisBinding> m_axesBindings;

		std::map<GamepadButton, ButtonBinding> m_buttonBindings;

		Uint16 m_gamepadStickDeadZone{0};

		void pollKeyboard();

		void pollGamepads();

		void pollBindings();

		static void poll();

		static SDL_GameController *getGamepad(int index);

		void shutdown();

		void addGamepad(const SDL_ControllerDeviceEvent &event);

		void removeGamepad(const SDL_ControllerDeviceEvent &event);

	public:
		static bool keyIsPressed(int scancode);

		static float getAxis(Axis axis, int gamepadIndex = 0);

		static float getAnyAxis(Axis axis);

		static bool gamepadButtonsIsPressed(GamepadButton buttons, int gamepadIndex = 0);

		static bool gamepadAnyButtonsIsPressed(GamepadButton buttons);

		static int getGamepadsCount();

		static std::string getGamepadName(int gamepadIndex);

		static void installEventListener(IInputEventListener *listener);

		static void uninstallEventListener(IInputEventListener *listener);

		static AxisBinding &bindAxis(Axis axis);

		static void unbindAxis(AxisBinding &binding);

		static void unbindAxis(Axis axis);

		static ButtonBinding &bindButton(GamepadButton button);

		static void unbindButton(ButtonBinding &binding);

		static void unbindButton(GamepadButton button);

		/// @param value in range [0, 32767]
		static void setGamepadDeadZone(Uint16 value);

		/// @return value in range [0, 32767]
		static Uint16 getGamepadDeadZone();

		static int getCurrentGamepad();
	};

}

#endif //NAGE_INPUT_HPP
