//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_APP_WINDOW_HPP
#define NAGE_APP_WINDOW_HPP

#include <SDL.h>
#include "viewport.hpp"

namespace nage::app {

	enum class WindowFullscreenMode {
		Windowed = SDL_FALSE,
		Full = SDL_WINDOW_FULLSCREEN,
		FullBorderless = SDL_WINDOW_FULLSCREEN_DESKTOP,

		Default = Windowed
	};

	class Window {
		friend class Application;

		SDL_Window *m_window;
		int m_posX{-1};
		int m_posY{-1};
		SDL_DisplayMode m_windowDisplayMode{};
		WindowFullscreenMode m_fullscreen{WindowFullscreenMode::Default};
		SDL_GLContext m_glContext{nullptr};
		Viewport m_viewport;

		Window(SDL_Window *window, bool debug);

	public:
		Window(const Window &) = delete;

		Window(Window &&) noexcept = delete;

		Window &operator=(const Window &) = delete;

		Window &operator=(Window &&) noexcept = delete;

		~Window();

		void setFullscreen(WindowFullscreenMode mode = WindowFullscreenMode::Default);

		[[nodiscard]] WindowFullscreenMode getFullscreenMode() const;

		[[nodiscard]] int getWidth() const;

		[[nodiscard]] int getHeight() const;

		void setSize(int width, int height);

		void setIcon(const SDL_Surface &surface);

		[[nodiscard]] const Viewport &getViewport() const;

		void swapWindow() const;

	private:
		void onWindowEvent(const SDL_WindowEvent &event);

		void updateView();
	};

}

#endif //NAGE_APP_WINDOW_HPP
