//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_APP_INSTANCE_HPP
#define NAGE_APP_INSTANCE_HPP

#include <memory>

namespace nage::app {

	class Instance {
	public:
		struct IHandle {
			virtual ~IHandle() = default;

			virtual bool open(const char *) = 0;

			virtual void close() = 0;
		};

	private:
		std::unique_ptr<IHandle> m_handle{nullptr};

		Instance();

		~Instance();

		static Instance &getInstance();

	public:
		Instance(const Instance &) = delete;

		Instance(Instance &&) noexcept = delete;

		Instance &operator=(const Instance &) = delete;

		Instance &operator=(Instance &&) noexcept = delete;

		static bool lockSingleInstance(const char id[128]);

	private:
		void resetHandle();
	};

}

#endif //NAGE_APP_INSTANCE_HPP
