//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_APP_OPTIONS_HPP
#define NAGE_APP_OPTIONS_HPP

#include <string>
#include <list>
#include <SDL.h>

namespace nage::app {

	struct Options {
		std::string title{"Unnamed application"};
		int defaultWidth{640};
		int defaultHeight{480};
		int defaultPositionX{SDL_WINDOWPOS_CENTERED};
		int defaultPositionY{SDL_WINDOWPOS_CENTERED};
		int glRedSize{8};
		int glGreenSize{8};
		int glBlueSize{8};
		int glAlphaSize{8};
		int glDepthSize{24};
		int glVersionMajor{2};
		int glVersionMinor{1};
		bool glDoubleBuffer{true};
		int glMultiSampleBuffers{1};
		int glMultiSampleSamples{16};
		bool glAcceleratedVisual{true};
		bool glDebug{false};
		bool allowHighDPI{true};
		bool allowResizable{true};

		enum ParseMode {
			PM_InvalidArgToStdOut = 0,
			PM_InvalidArgToStdErr,
			PM_InvalidArgToUnparsed,
			PM_InvalidArgIgnore,
		};

		void parse(int &argc, char **&argv,
				   ParseMode parseMode = PM_InvalidArgToStdErr,
				   std::list<std::string> *unparsedArgs = nullptr);

	};

}

#endif //NAGE_APP_OPTIONS_HPP
