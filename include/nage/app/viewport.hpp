//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_APP_VIEWPORT_HPP
#define NAGE_APP_VIEWPORT_HPP

namespace nage::app {

	struct Viewport {
		int left{0};
		int top{0};
		int right{0};
		int bottom{0};

		void setWidth(int v);

		[[nodiscard]] int getWidth() const;

		void setHeight(int v);

		[[nodiscard]] int getHeight() const;

		void translate(int dx, int dy);

		void setX(int v);

		[[nodiscard]] int getX() const;

		void setY(int v);

		[[nodiscard]] int getY() const;

		void apply() const;
	};

}

#endif //NAGE_APP_VIEWPORT_HPP
