//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_APP_APP_HPP
#define NAGE_APP_APP_HPP

#include "options.hpp"
#include <map>
#include <list>
#include <atomic>
#include <memory>

namespace nage::scene { class Scene; }

namespace nage::app {

	struct Interface {
		virtual ~Interface() = default;

		virtual bool filterEvent(const SDL_Event &event) {
			return false;
		}

		virtual void preUpdate() {}

		virtual void update(float deltaTime) {}

		virtual void preFrame() {}

		virtual void frame() const {}
	};

	class Window;

	class Application {
	public:
		enum AliasMode {
			AM_None = 0,
			AM_Aliased,
			AM_AntiAliased,
			AM_Multisample,
		};

		enum SwapInterval : int {
			SI_Immediately = 0,
			SI_VSync = 1,
			SI_Adaptive = -1,
		};

		using BucketId = size_t;

	private:
		std::atomic_bool m_run{false};
		std::atomic_bool m_restart{false};
		AliasMode m_aliasMode{AM_None};

		scene::Scene *m_scene{nullptr};

		std::unique_ptr<Window> m_window{nullptr};

	public:
		explicit Application(const Options &options = Options());

		Application(const Application &) = delete;

		Application(Application &&) noexcept = delete;

		Application &operator=(const Application &) = delete;

		Application &operator=(Application &&) noexcept = delete;

		~Application();

		static bool setSwapInterval(SwapInterval interval = SI_Immediately);

		static void quit();

		int start();

		static void restart();

		static void setScene(scene::Scene *scene);

		static scene::Scene *getScene();

		static void setAliasMode(AliasMode value = AM_None);

		static AliasMode getAliasMode();

		static int getOpenGLVersionMajor();

		static int getOpenGLVersionMinor();

		static Window *getWindow();

	private:
		void onEvent(const SDL_Event &event);

		void onUpdate(float deltaTime);

		void onRender() const;

		void swapWindow() const;

		void render() const;

		void loopStep(Uint64 &simulationTime, SDL_Event &e);

		void mainLoop();
	};

}

#endif //NAGE_APP_APP_HPP
