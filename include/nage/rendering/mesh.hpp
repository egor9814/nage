//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_MESH_HPP
#define NAGE_MESH_HPP

#include "vao.hpp"
#include "vbo.hpp"
#include "ebo.hpp"
#include "vertex.hpp"
#include <vector>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

namespace nage::rendering {

	class Mesh {
	public:
		using Vertices = std::vector<Vertex>;
		using Indices = std::vector<GLuint>;

		enum Mode : GLenum {
			M_Points = GL_POINTS,
			M_LineStrip = GL_LINE_STRIP,
			M_LineLoop = GL_LINE_LOOP,
			M_Lines = GL_LINES,
			M_LineStripAdjacency = GL_LINE_STRIP_ADJACENCY,
			M_LinesAdjacency = GL_LINES_ADJACENCY,
			M_TriangleStrip = GL_TRIANGLE_STRIP,
			M_TriangleFan = GL_TRIANGLE_FAN,
			M_Triangles = GL_TRIANGLES,
			M_TriangleStripAdjacency = GL_TRIANGLE_STRIP_ADJACENCY,
			M_TrianglesAdjacency = GL_TRIANGLES_ADJACENCY,
			M_Patches = GL_PATCHES,

			M_Default = M_Triangles
		};

	private:
		Vertices m_vertices;
		Indices m_indices;

		VAO m_vao;
		VBO m_vbo;
		EBO m_ebo;

		glm::mat4 m_transform{1};

		void recreateBuffers();

		void updateBuffers();

	public:
		glm::vec3 position{0};
		glm::vec3 rotation{0};
		glm::vec3 scale{1};
		glm::vec3 origin{0};

		Mode defaultMode{M_Default};

		Mesh();

		Mesh(Vertices vertices, Indices indices, Mode defaultMode = M_Default);

		Mesh(const Mesh &mesh);

		Mesh(Mesh &&mesh) noexcept;

		Mesh &operator=(const Mesh &mesh);

		Mesh &operator=(Mesh &&mesh) noexcept;

		void setData(const Vertices &vertices, const Indices &indices);

		[[nodiscard]] const Vertices &getVertices() const;

		[[nodiscard]] const Indices &getIndices() const;

		void bind() const;

		static void unbind();

		const glm::mat4x4 &applyTransformation();

		[[nodiscard]] const glm::mat4x4 &getTransform() const;

		void setTransform(const glm::mat4x4 &transform);

		void draw(Mode mode) const;

		void draw() const;

		void swap(Mesh &mesh);
	};

}

#endif //NAGE_MESH_HPP
