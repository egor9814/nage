//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_TEXTURES_HPP
#define NAGE_TEXTURES_HPP

#include <GL/glew.h>
#include <nage/utils/referencable.hpp>
#include <functional>
#include <SDL.h>

namespace nage::rendering {

	class Textures final {
		struct _impl : public utils::IReferencable {
			GLsizei count{0};
			GLuint *ids{nullptr};
			GLsizei *ws{nullptr};
			GLsizei *hs{nullptr};

			~_impl();
		} *m_impl;

	public:
		Textures();

		explicit Textures(GLsizei count);

		Textures(const Textures &textures);

		Textures(Textures &&textures) noexcept;

		Textures &operator=(const Textures &textures);

		Textures &operator=(Textures &&textures) noexcept;

		~Textures();

		void free();

		void create(GLsizei count);

		struct Options {
			enum Filter {
				F_Nearest = 0,
				F_Linear,
				F_Trilinear,

				F_Default = F_Nearest,
			} filter;

			int anisotropyFilter;

			enum Repeat {
				R_Repeat = 0,
				R_Clamp,
				R_ClampToZero,
				R_ClampToZeroAlpha,

				R_Default = R_Clamp,
			} repeat;

			Options() : Options(F_Default, -1, R_Default) {}

			explicit Options(Filter filter) : Options(filter, -1, R_Default) {}

			explicit Options(int anisotropyFilter) : Options(F_Default, -1, R_Default) {}

			explicit Options(Repeat repeat) : Options(F_Default, -1, repeat) {}

			Options(Filter filter, int anisotropyFilter) : Options(filter, anisotropyFilter, R_Default) {}

			Options(Filter filter, Repeat repeat) : Options(filter, -1, repeat) {}

			Options(int anisotropyFilter, Repeat repeat) : Options(F_Default, anisotropyFilter, repeat) {}

			Options(Filter filter, int anisotropyFilter, Repeat repeat) :
					filter(filter), anisotropyFilter(anisotropyFilter), repeat(repeat) {}
		} defaultOptions;

		void setOptions(GLuint index, const Options &options) const;

		static void setCurrentOptions(const Options &options);

		void loadFromPixelsRGBA(GLuint index, GLuint *pixels, GLsizei width, GLsizei height,
								const Options &options);

		void loadFromPixelsRGBA(GLuint index, GLuint *pixels, GLsizei width, GLsizei height);

		bool tryLoadTextureFromPixelsRGBA(GLuint index, GLuint *pixels, GLsizei width, GLsizei height,
										  const Options &options,
										  std::string *error = nullptr);

		bool tryLoadTextureFromPixelsRGBA(GLuint index, GLuint *pixels, GLsizei width, GLsizei height,
										  std::string *error = nullptr);

		void loadFromSurface(GLuint index, SDL_Surface *surface, const Options &options);

		void loadFromSurface(GLuint index, SDL_Surface *surface);

		bool tryLoadFromSurface(GLuint index, SDL_Surface *surface, const Options &options,
								std::string *error = nullptr);

		bool tryLoadFromSurface(GLuint index, SDL_Surface *surface,
								std::string *error = nullptr);

		void createFromColor(GLuint index,
							 GLsizei width, GLsizei height,
							 const Options &options,
							 const SDL_Color &color = {0x00, 0x00, 0x00, 0xff});

		void createFromColor(GLuint index,
							 GLsizei width, GLsizei height,
							 const SDL_Color &color = {0x00, 0x00, 0x00, 0xff});

		bool tryCreateFromColor(GLuint index,
								GLsizei width, GLsizei height,
								const Options &options,
								const SDL_Color &color = {0x00, 0x00, 0x00, 0xff},
								std::string *error = nullptr);

		bool tryCreateFromColor(GLuint index,
								GLsizei width, GLsizei height,
								const SDL_Color &color = {0x00, 0x00, 0x00, 0xff},
								std::string *error = nullptr);

		using ColorGenFunction =
		std::function<SDL_Color(GLsizei /*width*/, GLsizei /*height*/, GLsizei /*x*/, GLsizei /*y*/)>;

		void createFromColor(GLuint index,
							 GLsizei width, GLsizei height,
							 const Options &options,
							 ColorGenFunction &&f);

		void createFromColor(GLuint index,
							 GLsizei width, GLsizei height,
							 ColorGenFunction &&f);

		bool tryCreateFromColor(GLuint index,
								GLsizei width, GLsizei height,
								const Options &options,
								ColorGenFunction &&f,
								std::string *error = nullptr);

		bool tryCreateFromColor(GLuint index,
								GLsizei width, GLsizei height,
								ColorGenFunction &&f,
								std::string *error = nullptr);

		void bind(GLuint index) const;

		void bind(GLuint index, GLuint activeIndex) const;

		static void unbind();

		static void unbind(GLuint activeIndex);

		[[nodiscard]] bool isValid() const;

		[[nodiscard]] GLuint getId(GLuint index) const;

		[[nodiscard]] GLsizei getWidth(GLuint index) const;

		[[nodiscard]] GLsizei getHeight(GLuint index) const;

		[[nodiscard]] GLsizei getCount() const;

		void swap(Textures &textures);
	};

}

#endif //NAGE_TEXTURES_HPP
