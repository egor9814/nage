//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_BUFFER_OBJECT_HPP
#define NAGE_BUFFER_OBJECT_HPP

#include <GL/glew.h>
#include <atomic>
#include <string>
#include <nage/utils/referencable.hpp>

namespace nage::rendering {

	class BufferObject {
		struct _impl : public utils::IReferencable {
			GLuint id{0};
			GLenum type{0};

			~_impl() override;
		} *m_impl;

	public:
		BufferObject();

		BufferObject(const BufferObject &bo);

		BufferObject(BufferObject &&bo) noexcept;

		BufferObject &operator=(const BufferObject &bo);

		BufferObject &operator=(BufferObject &&bo) noexcept;

		virtual ~BufferObject();

		void free();

		void gen(GLenum type);

		bool tryGen(GLenum type, std::string *error = nullptr);

		void bind() const;

		static void unbind(GLenum type);

		void data(GLsizeiptr size, const void *data, GLenum usage = GL_STATIC_DRAW) const;

		[[nodiscard]] GLenum getType() const;

		bool isValid() const;

		void swap(BufferObject &bo);
	};

}

#endif //NAGE_BUFFER_OBJECT_HPP
