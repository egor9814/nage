//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_SHADER_HPP
#define NAGE_SHADER_HPP

#include <GL/glew.h>
#include <string>
#include <list>
#include <nage/utils/referencable.hpp>
#include <glm/vec4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>

namespace nage::rendering {

	class ShaderSource {
		struct _impl : public utils::IReferencable {
			GLuint id{0};

			~_impl() override;
		} *m_impl;

	public:
		ShaderSource();

		enum Type {
			Vertex = GL_VERTEX_SHADER,
			Fragment = GL_FRAGMENT_SHADER,
		};

		ShaderSource(const char *source, Type type);

		ShaderSource(const std::string &source, Type type);

		ShaderSource(const ShaderSource &s);

		ShaderSource(ShaderSource &&s) noexcept;

		ShaderSource &operator=(const ShaderSource &s);

		ShaderSource &operator=(ShaderSource &&s) noexcept;

		~ShaderSource();

		void free();

		bool operator==(const ShaderSource &s) const;

		bool operator!=(const ShaderSource &s) const;

		void load(const char *source, Type type);

		bool tryLoad(const char *source, Type type, std::string *error = nullptr);

		void load(const std::string &source, Type type);

		bool tryLoad(const std::string &source, Type type, std::string *error = nullptr);

		void loadFromPath(const std::string &path, Type type);

		bool tryLoadFromPath(const std::string &path, Type type, std::string *error = nullptr);

		[[nodiscard]] bool isValid() const;

		[[nodiscard]] GLuint getId() const;

		void swap(ShaderSource &s);
	};

	class Shader {
		struct _impl : public utils::IReferencable {
			GLuint id{0};
			std::list<ShaderSource> shaders;

			~_impl() override;
		} *m_impl;

	public:
		Shader();

		Shader(const Shader &shader);

		Shader(Shader &&shader) noexcept;

		Shader &operator=(const Shader &shader);

		Shader &operator=(Shader &&shader) noexcept;

		~Shader();

		void free();

		void create();

		void attach(const ShaderSource &s, bool takeOwnership = true);

		void detach(const ShaderSource &s);

		void attach(const char *source, ShaderSource::Type type);

		void attach(const std::string &source, ShaderSource::Type type);

		void attachFile(const std::string &path, ShaderSource::Type type);

		void link() const;

		bool tryLink(std::string *error) const;

		[[nodiscard]] bool isValid() const;

		[[nodiscard]] GLuint getId() const;

		GLint getAttribLocation(const char *name) const;

		GLint getUniformLocation(const char *name) const;

		class Attrib {
			GLint m_location;

		public:
			explicit Attrib(GLint location) : m_location(location) {}

			Attrib() : Attrib(-1) {}

			Attrib(const Attrib &) = default;

			Attrib(Attrib &&) noexcept = default;

			Attrib &operator=(const Attrib &) = default;

			Attrib &operator=(Attrib &&) noexcept = default;

			Attrib &operator=(GLint location) noexcept;

			~Attrib() = default;

			[[nodiscard]] bool isValid() const;

			[[nodiscard]] GLint getLocation() const;

			void enableVertexAttribArray() const;

			bool tryEnableVertexAttribArray(std::string *error = nullptr) const;

			void disableVertexAttribArray() const;

			bool tryDisableVertexAttribArray(std::string *error = nullptr) const;

			void setVertexAttribPointer(GLint size, GLenum type, GLboolean normalized, GLsizei stride,
										const GLvoid *pointer) const;

			bool trySetVertexAttribPointer(GLint size, GLenum type, GLboolean normalized, GLsizei stride,
										   const GLvoid *pointer,
										   std::string *error = nullptr) const;
		};

		static Attrib getAttrib(GLint location);

		Attrib getAttrib(const char *name) const;

		class Uniform {
			GLint m_location;

		public:
			explicit Uniform(GLint location) : m_location(location) {}

			Uniform() : Uniform(-1) {}

			Uniform(const Uniform &) = default;

			Uniform(Uniform &&) noexcept = default;

			Uniform &operator=(const Uniform &) = default;

			Uniform &operator=(Uniform &&) noexcept = default;

			Uniform &operator=(GLint location) noexcept;

			~Uniform() = default;

			[[nodiscard]] bool isValid() const;

			[[nodiscard]] GLint getLocation() const;

			void setVec4f(const glm::vec4 &v) const;

			void setVec3f(const glm::vec3 &v) const;

			void setVec2f(const glm::vec2 &v) const;

			void set1f(float v) const;

			void set1i(GLint v) const;

			void set1b(bool v) const;

			void setMat4f(const glm::mat4 &v, bool transpose) const;

			void setMat3f(const glm::mat3 &v, bool transpose) const;

			void setMat4f(const glm::mat4 &v) const;

			void setMat3f(const glm::mat3 &v) const;
		};

		static Uniform getUniform(GLint location);

		Uniform getUniform(const char *name) const;

		template<typename T>
		using UniformFunc = void (Uniform::*)(T) const;

		template<typename T>
		struct TypedUniformFunc;

		template<>
		struct TypedUniformFunc<float> {
			static constexpr auto FUNC = &Uniform::set1f;
		};
		template<>
		struct TypedUniformFunc<int> {
			static constexpr auto FUNC = &Uniform::set1i;
		};
		template<>
		struct TypedUniformFunc<bool> {
			static constexpr auto FUNC = &Uniform::set1b;
		};
		template<>
		struct TypedUniformFunc<glm::vec4> {
			static constexpr auto FUNC = &Uniform::setVec4f;
		};
		template<>
		struct TypedUniformFunc<glm::vec3> {
			static constexpr auto FUNC = &Uniform::setVec3f;
		};
		template<>
		struct TypedUniformFunc<glm::vec2> {
			static constexpr auto FUNC = &Uniform::setVec2f;
		};
		template<>
		struct TypedUniformFunc<glm::mat4> {
			static constexpr auto FUNC = static_cast<UniformFunc<const glm::mat4 &>>(&Uniform::setMat4f);
		};
		template<>
		struct TypedUniformFunc<glm::mat3> {
			static constexpr auto FUNC = static_cast<UniformFunc<const glm::mat3 &>>(&Uniform::setMat3f);
		};

		template<typename T>
		struct TypedUniform {
			Uniform uniform;

			void set(T &&value) const {
				((&uniform)->*(TypedUniformFunc<T>::FUNC))(std::move(value));
			}

			const TypedUniform &operator=(T &&value) const { // NOLINT(misc-unconventional-assign-operator)
				set(std::move(value));
				return *this;
			}

			void set(const T &value) const {
				((&uniform)->*(TypedUniformFunc<T>::FUNC))(value);
			}

			const TypedUniform &operator=(const T &value) const { // NOLINT(misc-unconventional-assign-operator)
				set(value);
				return *this;
			}
		};

		using FloatUniform = TypedUniform<float>;
		using IntUniform = TypedUniform<int>;
		using BoolUniform = TypedUniform<bool>;
		using Vec4Uniform = TypedUniform<glm::vec4>;
		using Vec3Uniform = TypedUniform<glm::vec3>;
		using Vec2Uniform = TypedUniform<glm::vec2>;
		using Mat4Uniform = TypedUniform<glm::mat4>;
		using Mat3Uniform = TypedUniform<glm::mat3>;

		using Sampler2DUniform = IntUniform;

		template<typename T>
		TypedUniform<T> getTypedUniform(const char *name) const {
			return TypedUniform<T>{getUniform(name)};
		}

		template<typename T>
		void setTypedUniform(const char *name, TypedUniform<T> &out) const {
			out = getTypedUniform<T>(name);
		}

		[[nodiscard]] Mat4Uniform getModelMatrix() const;

		[[nodiscard]] Mat4Uniform getViewMatrix() const;

		[[nodiscard]] Mat4Uniform getProjectionMatrix() const;

		void use() const;

		static void unuse();

		void swap(Shader &shader);
	};
}

#endif //NAGE_SHADER_HPP
