//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_MATERIAL_HPP
#define NAGE_MATERIAL_HPP

#include <string>
#include <map>
#include <glm/mat4x4.hpp>

namespace nage::rendering {

	struct Material {
		std::string name;

		virtual void updateUniforms() const {}

		virtual void updateModelMatrix(const glm::mat4 &matrix) const {}

		virtual void updateViewMatrix(const glm::mat4 &matrix) const {}

		virtual void updateProjectionMatrix(const glm::mat4 &matrix) const {}

		virtual void bindTextures() const {}

		virtual void unbindTextures() const {}

		virtual void enableAttribs() const {}

		virtual void disableAttribs() const {}
	};

	using MaterialId = int64_t;

	using MaterialLib = std::map<MaterialId, Material *>;

}

#endif //NAGE_MATERIAL_HPP
