//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_VAO_HPP
#define NAGE_VAO_HPP

#include <GL/glew.h>
#include <atomic>
#include <string>
#include <nage/utils/referencable.hpp>

namespace nage::rendering {

	class VAO {
		struct _impl : public utils::IReferencable {
			GLuint id{0};

			~_impl() override;
		} *m_impl;

	public:
		VAO();

		VAO(const VAO &vao);

		VAO(VAO &&vao) noexcept;

		VAO &operator=(const VAO &vao);

		VAO &operator=(VAO &&vao) noexcept;

		~VAO();

		void free();

		void create();

		bool tryCreate(std::string *error = nullptr);

		void bind() const;

		static void unbind();

		[[nodiscard]] bool isValid() const;

		void swap(VAO &vao);
	};

}

#endif //NAGE_VAO_HPP
