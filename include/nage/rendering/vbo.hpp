//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_VBO_HPP
#define NAGE_VBO_HPP

#include "buffer_object.hpp"

namespace nage::rendering {

	class VBO : public BufferObject {
	public:
		VBO() = default;

		VBO(const VBO &vbo) = default;

		VBO(VBO &&vbo) noexcept = default;

		VBO &operator=(const VBO &vbo) = default;

		VBO &operator=(VBO &&vbo) noexcept = default;

		~VBO() override = default;

		void gen();

		bool tryGen(std::string *error = nullptr);

		static void unbind();
	};

}

#endif //NAGE_VBO_HPP
