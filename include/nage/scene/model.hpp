//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_MODEL_HPP
#define NAGE_MODEL_HPP

#include <nage/rendering/mesh.hpp>
#include <nage/rendering/material.hpp>
#include <nage/rendering/shader.hpp>
#include <list>
#include "camera.hpp"

namespace nage::scene {

	class Model {
		glm::mat4x4 m_transform{1};

	public:
		using MeshList = std::list<rendering::Mesh>;

		using Meshes = std::map<rendering::MaterialId, MeshList>;

		Model();

		Model(const Model &model);

		Model(Model &&model) noexcept;

		Model &operator=(const Model &model);

		Model &operator=(Model &&model) noexcept;

		~Model();

		rendering::MaterialLib materials;
		Meshes meshes;

		glm::vec3 position{0};
		glm::vec3 rotation{0};
		glm::vec3 scale{1};

		void appendMesh(rendering::MaterialId material, const rendering::Mesh &mesh);

		void appendMeshes(rendering::MaterialId material, const MeshList &meshList);

		const glm::mat4x4 &applyTransformation();

		[[nodiscard]] const glm::mat4x4 &getTransform() const;

		void draw(Camera *camera = nullptr, rendering::Shader *shader = nullptr) const;

		void swap(Model &model);
	};

}

#endif //NAGE_MODEL_HPP
