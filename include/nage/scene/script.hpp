//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_SCRIPT_HPP
#define NAGE_SCRIPT_HPP

#include <SDL_events.h>

namespace nage::scene {

	struct IScript {
		virtual ~IScript() = default;

		virtual void onActivated() = 0;

		virtual void onDeactivated() = 0;

		virtual void onStart() = 0;

		virtual void onUpdate(float deltaTime) = 0;

		virtual bool onFilterEvent(const SDL_Event &event) = 0;

	};

}

#endif //NAGE_SCRIPT_HPP
