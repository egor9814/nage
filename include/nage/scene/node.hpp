//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_NODE_HPP
#define NAGE_NODE_HPP

#include <list>
#include <atomic>
#include <SDL_events.h>
#include "model.hpp"
#include "script.hpp"
#include <nage/rendering/shader.hpp>

namespace nage::scene {

	class Node : protected IScript {
		friend class Scene;

		std::atomic_bool m_activated{false};
		std::atomic_bool m_started{false};

	public:
		Node() = default;

		std::list<Node *> children;

		bool isActive() const;

		void setActive(bool value = true);

		Model *model{nullptr};
		rendering::Shader *modelShader{nullptr};

		Camera::Layer layer{Camera::LAYER_WORLD};

		using BacketId = int;
		BacketId bucket{0};

	private:
		void activated();

		void deactivated();

		void start();

		void update(float deltaTime);

		bool filterEvent(const SDL_Event &event);

		void render(Camera *camera, rendering::Shader *shader);
	};

}

#endif //NAGE_NODE_HPP
