//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_CAMERA_HPP
#define NAGE_CAMERA_HPP

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <set>
#include <nage/app/viewport.hpp>

namespace nage::scene {

	class Camera {
	public:
		enum ProjectionMode {
			PM_Undefined,
			PM_Orthographic,
			PM_Perspective
		};

	private:
		glm::mat4x4 m_viewMatrix{1};
		glm::mat4x4 m_projectionMatrix{1};
		ProjectionMode m_projectionMode{PM_Undefined};

		glm::vec3 m_front{0};
		glm::vec3 m_right{0};
		glm::vec3 m_up{0};

		app::Viewport m_viewport;

		float m_aspectRatio{1};

	public:
		Camera();

		glm::vec3 position{0};
		glm::vec3 worldUp{0, 1, 0};

		struct Rotation {
			union {
				float x;
				float pitch;
			};
			union {
				float y;
				float yaw;
			};
		} rotation{0, 0};

		float nearPlane{0.1f};
		float farPlane{1000};
		float fov{90};
		float orthographicSize{1};

		using Layer = int;
		static constexpr Layer LAYER_BACKGROUND = 0;
		static constexpr Layer LAYER_WORLD = 1;
		static constexpr Layer LAYER_UI = 5;
		static constexpr Layer LAYER_USER = 10;
		std::set<Layer> layers{LAYER_BACKGROUND, LAYER_WORLD, LAYER_UI, LAYER_USER};

		const glm::mat4x4 &lookAt();

		[[nodiscard]] const glm::mat4x4 &getViewMatrix() const;

		const glm::mat4x4 &perspective();

		const glm::mat4x4 &orthographic();

		[[nodiscard]] const glm::mat4x4 &getProjectionMatrix() const;

		const glm::mat4x4 &updateCurrentProjection();

		[[nodiscard]] ProjectionMode getProjectionMode() const;

		void setViewportSize(int width, int height);

		void setViewportPosition(int x, int y);

		[[nodiscard]] int getViewportWidth() const;

		[[nodiscard]] int getViewportHeight() const;

		[[nodiscard]] int getViewportX() const;

		[[nodiscard]] int getViewportY() const;

		[[nodiscard]] const app::Viewport &getViewport() const;
	};

}

#endif //NAGE_CAMERA_HPP
