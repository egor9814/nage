//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_SCENE_HPP
#define NAGE_SCENE_HPP

#include <vector>
#include "node.hpp"

namespace nage::app { class Application; }

namespace nage::scene {

	class Scene {
		friend class ::nage::app::Application;

	public:
		Scene() {
			updateBuckets.push_back(0);
		}

		std::list<Node *> nodes;

		std::vector<Camera *> cameras;

		std::vector<Node::BacketId> updateBuckets;

		void activateAll();

	private:
		bool filterEvent(const SDL_Event &event);

		void update(float deltaTime);

		void render(rendering::Shader *shader) const;
	};

}

#endif //NAGE_SCENE_HPP
