//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_OBJECT_HPP
#define NAGE_OBJECT_HPP

#include "script.hpp"
#include "node.hpp"
#include <list>

namespace nage::scene {

	class Object : public Node {
	public:
		Object() = default;

	protected:
		void onActivated() override;

		void onDeactivated() override;

		void onStart() override;

		void onUpdate(float deltaTime) override;

		bool onFilterEvent(const SDL_Event &event) override;
	};

}

#endif //NAGE_OBJECT_HPP
