//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_REFERENCABLE_HPP
#define NAGE_REFERENCABLE_HPP

#include <atomic>

namespace nage::utils {

	using atomic_uint64_t = std::atomic_uint_fast64_t;

	class IReferencable {
		atomic_uint64_t m_refs;

	public:
		IReferencable();

		bool unref();

		void ref();

		atomic_uint64_t::value_type getCount() const;

	protected:
		virtual ~IReferencable();
	};

}

#endif //NAGE_REFERENCABLE_HPP
