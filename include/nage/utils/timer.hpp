//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_TIMER_HPP
#define NAGE_TIMER_HPP

#include <SDL.h>

namespace nage::utils {

	class Timer {
		Uint64 m_startTicks;
		Uint64 m_pausedTicks;
		bool m_paused;
		bool m_started;

	public:
		Timer();

		void start();

		void stop();

		void pause();

		void resume();

		[[nodiscard]] Uint64 time() const;

		[[nodiscard]] bool isStarted() const;

		[[nodiscard]] bool isPaused() const;
	};

}

#endif //NAGE_TIMER_HPP
