//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_IMAGE_HPP
#define NAGE_IMAGE_HPP

#include <SDL.h>
#include "referencable.hpp"
#include <string>

namespace nage::utils {

	class Image {
		struct _impl : public IReferencable {
			SDL_Surface *surface;
			bool free;

			_impl(SDL_Surface *surface, bool free);

			~_impl() override;
		} *m_impl;

		static _impl *createImpl(SDL_Surface *surface, bool takeOwnership = true);

	public:
		Image();

		explicit Image(SDL_Surface *surface, bool takeOwnership = true);

		Image(const Image &image);

		Image(Image &&image) noexcept;

		Image &operator=(const Image &image);

		Image &operator=(Image &&image) noexcept;

		~Image();

		[[nodiscard]] bool isValid() const;

		void free();

		void open(const std::string &path);

		bool tryOpen(const std::string &path, std::string *error = nullptr);

		[[nodiscard]] SDL_Surface *getSurface() const;

		[[nodiscard]] Image convert(SDL_PixelFormat *format, Uint32 flags = 0) const;

		[[nodiscard]] Image convertFormat(SDL_PixelFormatEnum format, Uint32 flags = 0) const;

		void swap(Image &image);
	};

}

#endif //NAGE_IMAGE_HPP
