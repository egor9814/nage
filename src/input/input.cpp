//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <nage/input/input.hpp>

namespace nage::input {

#pragma region AxisBinding

	AxisBinding::AxisBinding(Axis axis) : m_axis(axis) {}

	AxisBinding::AxisBinding() : AxisBinding(Axis::Invalid) {}

	AxisBinding &AxisBinding::addKey(Scancode scancode, float value) {
		m_keys[scancode] = value;
		return *this;
	}

	AxisBinding &AxisBinding::removeKey(Scancode scancode) {
		m_keys.erase(scancode);
		return *this;
	}

	Axis AxisBinding::getAxis() const {
		return m_axis;
	}

	void AxisBinding::emit() {
		if (m_axis < Axis::LeftX || m_axis > Axis::TriggerRight)
			return;
		float result = 0;
		if (!m_keys.empty()) {
			for (const auto &it: m_keys) {
				if (Input::keyIsPressed(it.first)) {
					result += it.second;
				}
			}
		}
		if (result > 1)
			result = 1;
		if (result < -1)
			result = -1;
		Input::get().m_virtualGamepad.axes.array[int(m_axis)] = result;
	}

#pragma endregion //AxisBinding

#pragma region ButtonBinding

	ButtonBinding::ButtonBinding(GamepadButton button) : m_button(button) {}

	ButtonBinding::ButtonBinding() : ButtonBinding(GamepadButton::None) {}

	ButtonBinding &ButtonBinding::addKey(Scancode scancode) {
		m_keys.insert(scancode);
		return *this;
	}

	ButtonBinding &ButtonBinding::removeKey(Scancode scancode) {
		m_keys.erase(scancode);
		return *this;
	}

	GamepadButton ButtonBinding::getButton() const {
		return m_button;
	}

	void ButtonBinding::emit() {
		if (m_button == GamepadButton::None)
			return;
		if (!m_keys.empty()) {
			for (const auto &it: m_keys) {
				if (Input::keyIsPressed(it)) {
					Input::get().m_virtualGamepad.buttons.mask |= m_button;
					return;
				}
			}
		}
		Input::get().m_virtualGamepad.buttons.mask &= ~m_button;
	}

#pragma endregion //ButtonBinding

#pragma region Input

	Input &Input::get() {
		static Input i;
		return i;
	}

	void Input::pollKeyboard() {
		m_keyboard = SDL_GetKeyboardState(nullptr);
	}

	float fixSticks(SDL_GameController *c, SDL_GameControllerAxis axis, int deadZone) {
		auto v = int(SDL_GameControllerGetAxis(c, axis));
		if (-deadZone <= v && v <= deadZone)
			return 0;
		if (v < 0)
			return float(v) / 32768;
		return float(v) / 32767;
	}

	void Input::pollGamepads() {
		{
			lock_t lock{m_mtx};
			auto count = SDL_NumJoysticks();
			if (count != m_g.size()) {
				if (count < m_g.size()) {
					std::list<SDL_GameController *> forClose;
					for (const auto &it: m_g) {
						auto c = it.first;
						if (!SDL_GameControllerGetAttached(c))
							forClose.push_back(c);
					}
					for (const auto &it: forClose) {
						m_g.erase(it);
						SDL_GameControllerClose(it);
					}
					int i = 0;
					for (auto &it: m_g) {
						it.second.index = i++;
					}
					for (const auto &it: m_inputEventListeners)
						it->onGamepadDetached();
				}
				if (count > m_g.size()) {
					int offset = 0;
					for (int i = int(m_g.size()); i < count; i++) {
						auto c = SDL_GameControllerOpen(i);
						if (!c) {
							offset++;
							continue;
						}
						auto &g = m_g[c];
						g.index = i - offset;
						auto j = SDL_GameControllerGetJoystick(c);
						g.name = SDL_JoystickName(j);
					}
					for (const auto &it: m_inputEventListeners)
						it->onGamepadAttached();
				}
			}
		}

		for (auto &it: m_g) {
			auto &a = it.second.axes;
			a.lx = fixSticks(it.first, SDL_CONTROLLER_AXIS_LEFTX, int(m_gamepadStickDeadZone));
			a.rx = fixSticks(it.first, SDL_CONTROLLER_AXIS_RIGHTX, int(m_gamepadStickDeadZone));
			a.ly = fixSticks(it.first, SDL_CONTROLLER_AXIS_LEFTY, int(m_gamepadStickDeadZone));
			a.ry = fixSticks(it.first, SDL_CONTROLLER_AXIS_RIGHTY, int(m_gamepadStickDeadZone));
			a.lt = fixSticks(it.first, SDL_CONTROLLER_AXIS_TRIGGERLEFT, 0);
			a.rt = fixSticks(it.first, SDL_CONTROLLER_AXIS_TRIGGERRIGHT, 0);

			auto &mask = it.second.buttons.mask;
			mask = GamepadButton::None;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_A))
				mask |= GamepadButton::A;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_B))
				mask |= GamepadButton::B;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_X))
				mask |= GamepadButton::X;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_Y))
				mask |= GamepadButton::Y;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_BACK))
				mask |= GamepadButton::BACK;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_GUIDE))
				mask |= GamepadButton::GUIDE;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_START))
				mask |= GamepadButton::START;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_LEFTSTICK))
				mask |= GamepadButton::LEFT_STICK;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_RIGHTSTICK))
				mask |= GamepadButton::RIGHT_STICK;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_LEFTSHOULDER))
				mask |= GamepadButton::LEFT_SHOULDER;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_RIGHTSHOULDER))
				mask |= GamepadButton::RIGHT_SHOULDER;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_DPAD_UP))
				mask |= GamepadButton::DPAD_UP;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_DPAD_DOWN))
				mask |= GamepadButton::DPAD_DOWN;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_DPAD_LEFT))
				mask |= GamepadButton::DPAD_LEFT;
			if (SDL_GameControllerGetButton(it.first, SDL_CONTROLLER_BUTTON_DPAD_RIGHT))
				mask |= GamepadButton::DPAD_RIGHT;
		}
	}

	void Input::pollBindings() {
		lock_t lock{m_mtx};
		for (auto &it: m_axesBindings) {
			it.second.emit();
		}
		for (auto &it: m_buttonBindings) {
			it.second.emit();
		}
	}

	void Input::poll() {
		auto &i = get();
		i.pollKeyboard();
		i.pollGamepads();
		i.pollBindings();
	}

	SDL_GameController *Input::getGamepad(int index) {
		for (const auto &it: get().m_g) {
			if (it.second.index == index)
				return it.first;
		}
		return nullptr;
	}

	void Input::shutdown() {
		for (const auto &it: m_g) {
			SDL_GameControllerClose(it.first);
		}
		m_g.clear();
	}

	void Input::addGamepad(const SDL_ControllerDeviceEvent &event) {
		lock_t lock{m_mtx};
		auto c = SDL_GameControllerOpen(event.which);
		if (!c) {
			return;
		}
		auto index = int(m_g.size());
		if (index != event.which)
			return;
		auto &g = m_g[c];
		g.index = index;
		auto j = SDL_GameControllerGetJoystick(c);
		g.name = SDL_JoystickName(j);
		for (const auto &it: m_inputEventListeners)
			it->onGamepadAttached();
	}

	void Input::removeGamepad(const SDL_ControllerDeviceEvent &event) {
		lock_t lock{m_mtx};
		auto c = getGamepad(event.which);
		if (!c)
			return;
		m_g.erase(c);
		SDL_GameControllerClose(c);
		int index = 0;
		for (auto &it: m_g) {
			it.second.index = index++;
		}
		for (const auto &it: m_inputEventListeners)
			it->onGamepadDetached();
	}

	bool Input::keyIsPressed(int scancode) {
		auto &i = get();
		if (!i.m_keyboard)
			return false;
		return i.m_keyboard[scancode];
	}

	float Input::getAxis(Axis axis, int gamepadIndex) {
		if (gamepadIndex >= getGamepadsCount())
			gamepadIndex = -1;
		if (gamepadIndex <= -1)
			return get().m_virtualGamepad.axes.array[int(axis)];
		auto g = getGamepad(gamepadIndex);
		if (!g)
			return 0;
		return get().m_g[g].axes.array[int(axis)];
	}

	float Input::getAnyAxis(Axis axis) {
		auto &i = get();
		float result = i.m_virtualGamepad.axes.array[int(axis)];
		for (const auto &g: i.m_g) {
			result += g.second.axes.array[int(axis)];
		}
		if (result > 1)
			result = 1;
		if (result < -1)
			result = -1;
		return result;
	}

	bool Input::gamepadButtonsIsPressed(GamepadButton buttons, int gamepadIndex) {
		if (gamepadIndex >= getGamepadsCount())
			gamepadIndex = -1;
		if (gamepadIndex <= -1)
			return (get().m_virtualGamepad.buttons.mask & buttons) == buttons;
		auto g = getGamepad(gamepadIndex);
		if (!g)
			return false;
		return (get().m_g[g].buttons.mask & buttons) == buttons;
	}

	bool Input::gamepadAnyButtonsIsPressed(GamepadButton buttons) {
		auto &i = get();
		for (const auto &g: i.m_g) {
			if ((g.second.buttons.mask & buttons) == buttons) {
				return true;
			}
		}
		return (i.m_virtualGamepad.buttons.mask & buttons) == buttons;
	}

	int Input::getGamepadsCount() {
		return int(get().m_g.size());
	}

	std::string Input::getGamepadName(int gamepadIndex) {
		if (gamepadIndex >= getGamepadsCount())
			gamepadIndex = -1;
		if (gamepadIndex <= -1)
			return get().m_virtualGamepad.name;
		auto g = getGamepad(gamepadIndex);
		if (!g)
			return "";
		return get().m_g[g].name;
	}

	void Input::installEventListener(IInputEventListener *listener) {
		if (!listener)
			return;
		auto &i = get();
		lock_t lock{i.m_mtx};
		i.m_inputEventListeners.push_back(listener);
	}

	void Input::uninstallEventListener(IInputEventListener *listener) {
		if (!listener)
			return;
		auto &i = get();
		lock_t lock{i.m_mtx};
		i.m_inputEventListeners.remove(listener);
	}

	AxisBinding &Input::bindAxis(Axis axis) {
		auto &i = get();
		lock_t lock{i.m_mtx};
		if (!i.m_axesBindings.contains(axis))
			i.m_axesBindings[axis].m_axis = axis;
		return i.m_axesBindings[axis];
	}

	void Input::unbindAxis(AxisBinding &binding) {
		auto &i = get();
		lock_t lock{i.m_mtx};
		i.m_axesBindings.erase(binding.m_axis);
	}

	void Input::unbindAxis(Axis axis) {
		auto &i = get();
		lock_t lock{i.m_mtx};
		i.m_axesBindings.erase(axis);
	}

	ButtonBinding &Input::bindButton(GamepadButton button) {
		auto &i = get();
		lock_t lock{i.m_mtx};
		if (!i.m_buttonBindings.contains(button))
			i.m_buttonBindings[button].m_button = button;
		return i.m_buttonBindings[button];
	}

	void Input::unbindButton(ButtonBinding &binding) {
		auto &i = get();
		lock_t lock{i.m_mtx};
		i.m_buttonBindings.erase(binding.m_button);
	}

	void Input::unbindButton(GamepadButton button) {
		auto &i = get();
		lock_t lock{i.m_mtx};
		i.m_buttonBindings.erase(button);
	}

	void Input::setGamepadDeadZone(Uint16 value) {
		if (value > 32767)
			value = 32767;
		Input::get().m_gamepadStickDeadZone = value;
	}

	Uint16 Input::getGamepadDeadZone() {
		return Input::get().m_gamepadStickDeadZone;
	}

	int Input::getCurrentGamepad() {
		return getGamepadsCount() ? 0 : -1;
	}

#pragma endregion //Input

}