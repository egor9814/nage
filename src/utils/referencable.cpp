//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <nage/utils/referencable.hpp>

namespace nage::utils {

	IReferencable::IReferencable() : m_refs(1) {}

	bool IReferencable::unref() {
		if (auto count = m_refs.fetch_sub(1) - 1; count == 0) {
			delete this;
			return true;
		}
		return false;
	}

	void IReferencable::ref() {
		m_refs++;
	}

	atomic_uint64_t::value_type IReferencable::getCount() const {
		return m_refs.load();
	}

	IReferencable::~IReferencable() = default;

}
