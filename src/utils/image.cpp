//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <nage/utils/image.hpp>
#include <SDL_image.h>
#include "../common.hpp"

namespace nage::utils {

	Image::_impl::~_impl() {
		if (surface && free)
			SDL_FreeSurface(surface);
	}

	Image::_impl::_impl(SDL_Surface *surface, bool free) : surface(surface), free(free) {}

	Image::_impl *Image::createImpl(SDL_Surface *surface, bool takeOwnership) {
		return surface ? new _impl{surface, takeOwnership} : nullptr;
	}

	Image::Image() : m_impl(nullptr) {}

	Image::Image(SDL_Surface *surface, bool takeOwnership) :
			m_impl(createImpl(surface, takeOwnership)) {}

	Image::Image(const Image &image) : m_impl(image.m_impl) {
		if (m_impl)
			m_impl->ref();
	}

	Image::Image(Image &&image) noexcept: m_impl(nullptr) {
		swap(image);
	}

	Image &Image::operator=(const Image &image) {
		if (this != &image) {
			free();
			m_impl = image.m_impl;
			if (m_impl)
				m_impl->ref();
		}
		return *this;
	}

	Image &Image::operator=(Image &&image) noexcept {
		if (this != &image) {
			free();
			swap(image);
		}
		return *this;
	}

	Image::~Image() {
		free();
	}

	bool Image::isValid() const {
		return m_impl != nullptr;
	}

	void Image::free() {
		if (m_impl)
			m_impl->unref();
		m_impl = nullptr;
	}

	void Image::open(const std::string &path) {
		free();
		if (path.ends_with(".bmp"))
			m_impl = createImpl(SDL_LoadBMP(path.data()));
		if (!m_impl)
			m_impl = createImpl(IMG_Load(path.data()));
		if (!m_impl)
			throw error("Unable to load image %s! SDL Error: %s", path.data(), SDL_GetError());
	}

	bool Image::tryOpen(const std::string &path, std::string *error) {
		COMMON_TRY(open(path))
	}

	SDL_Surface *Image::getSurface() const {
		return m_impl ? m_impl->surface : nullptr;
	}

	Image Image::convert(SDL_PixelFormat *format, Uint32 flags) const {
		return Image(SDL_ConvertSurface(m_impl->surface, format, flags));
	}

	Image Image::convertFormat(SDL_PixelFormatEnum format, Uint32 flags) const {
		return Image(SDL_ConvertSurfaceFormat(m_impl->surface, format, flags));
	}

	void Image::swap(Image &image) {
		std::swap(m_impl, image.m_impl);
	}

}