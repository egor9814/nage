//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <nage/utils/timer.hpp>

namespace nage::utils {

	Timer::Timer() : m_startTicks(0), m_pausedTicks(0), m_started(false), m_paused(false) {}

	void Timer::start() {
		m_started = true;
		m_paused = false;
		m_pausedTicks = 0;
		m_startTicks = SDL_GetTicks64();
	}

	void Timer::stop() {
		m_started = false;
		m_paused = false;
	}

	void Timer::pause() {
		if (m_started && !m_paused) {
			m_pausedTicks = SDL_GetTicks64() - m_startTicks;
			m_startTicks = 0;
			m_paused = true;
		}
	}

	void Timer::resume() {
		if (m_started && m_paused) {
			m_startTicks = SDL_GetTicks64() - m_pausedTicks;
			m_pausedTicks = 0;
			m_paused = false;
		}
	}

	Uint64 Timer::time() const {
		if (!m_started)
			return 0;
		if (m_paused)
			return m_pausedTicks;
		return SDL_GetTicks64() - m_startTicks;
	}

	bool Timer::isStarted() const {
		return m_started;
	}

	bool Timer::isPaused() const {
		return m_paused;
	}

}