//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <GL/glew.h>
#include <nage/scene/node.hpp>

namespace nage::scene {

	bool Node::isActive() const {
		return m_activated.load();
	}

	void Node::setActive(bool value) {
		if (value == m_activated.load())
			return;
		m_activated = value;
		if (value) {
			if (!m_started.load()) {
				m_started = true;
				start();
			}
			activated();
		} else {
			deactivated();
		}
	}

	void Node::activated() {
		onActivated();
	}

	void Node::deactivated() {
		onDeactivated();
	}

	void Node::start() {
		onStart();
		for (auto node: children) {
			node->start();
		}
	}

	void Node::update(float deltaTime) {
		onUpdate(deltaTime);
		for (auto node: children) {
			node->update(deltaTime);
		}
	}

	bool Node::filterEvent(const SDL_Event &event) {
		if (onFilterEvent(event))
			return true;
		for (auto node: children) {
			if (node->filterEvent(event))
				return true;
		}
		return false;
	}

	void Node::render(Camera *camera, rendering::Shader *shader) {
		if (camera->layers.contains(layer) && model) {
			model->draw(camera, shader ? shader : modelShader);
		}
		for (auto node: children) {
			node->render(camera, shader);
		}
	}

}
