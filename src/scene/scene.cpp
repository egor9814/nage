//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <GL/glew.h>
#include <nage/scene/scene.hpp>

namespace nage::scene {

	void activateRecursive(std::list<Node *> &nodes) {
		for (auto node: nodes) {
			node->setActive();
			activateRecursive(node->children);
		}
	}

	void Scene::activateAll() {
		activateRecursive(nodes);
	}

	bool Scene::filterEvent(const SDL_Event &event) {
		for (auto node: nodes) {
			if (node->isActive() && node->filterEvent(event))
				return true;
		}
		return false;
	}

	void Scene::update(float deltaTime) {
		for (auto bucket: updateBuckets) {
			for (auto node: nodes) {
				if (node->isActive() && node->bucket == bucket)
					node->update(deltaTime);
			}
		}
	}

	void Scene::render(rendering::Shader *shader) const {
		if (cameras.empty() || nodes.empty())
			return;
		for (auto camera: cameras) {
			camera->getViewport().apply();
			for (auto node: nodes) {
				if (node->isActive())
					node->render(camera, shader);
			}
		}
	}

}