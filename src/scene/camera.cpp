//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <nage/scene/camera.hpp>
#include <glm/ext.hpp>

namespace nage::scene {

	Camera::Camera() {
		lookAt();
		perspective();
	}

	const glm::mat4x4 &Camera::lookAt() {
		auto pc = cos(rotation.pitch);
		m_front.x = cos(rotation.yaw) * pc;
		m_front.y = sin(rotation.pitch);
		m_front.z = sin(rotation.yaw) * pc;
		m_front = glm::normalize(m_front);

		m_right = glm::normalize(glm::cross(m_front, worldUp));

		m_up = glm::normalize(glm::cross(m_right, m_front));

		m_viewMatrix = glm::lookAt(position, position + m_front, m_up);
		return m_viewMatrix;
	}

	const glm::mat4x4 &Camera::getViewMatrix() const {
		return m_viewMatrix;
	}

	const glm::mat4x4 &Camera::perspective() {
		m_projectionMatrix = glm::perspective(
				glm::radians(fov),
				m_aspectRatio,
				nearPlane,
				farPlane
		);
		m_projectionMode = PM_Perspective;
		return m_projectionMatrix;
	}

	const glm::mat4x4 &Camera::orthographic() {
		auto h2 = orthographicSize / 2;
		auto w2 = m_aspectRatio * h2;
		m_projectionMatrix = glm::ortho(-w2, w2, -h2, h2, -farPlane, farPlane);
		m_projectionMode = PM_Orthographic;
		return m_projectionMatrix;
	}

	const glm::mat4x4 &Camera::getProjectionMatrix() const {
		return m_projectionMatrix;
	}

	const glm::mat4x4 &Camera::updateCurrentProjection() {
		switch (m_projectionMode) {
			default:
				break;
			case PM_Orthographic:
				orthographic();
				break;
			case PM_Perspective:
				perspective();
				break;
		}
		return m_projectionMatrix;
	}

	Camera::ProjectionMode Camera::getProjectionMode() const {
		return m_projectionMode;
	}

	void Camera::setViewportSize(int width, int height) {
		m_viewport.setWidth(width);
		m_viewport.setHeight(height);
		if (height != 0)
			m_aspectRatio = float(width) / float(height);
		else
			m_aspectRatio = 1;
		updateCurrentProjection();
	}

	void Camera::setViewportPosition(int x, int y) {
		m_viewport.setX(x);
		m_viewport.setY(y);
	}

	int Camera::getViewportWidth() const {
		return m_viewport.getWidth();
	}

	int Camera::getViewportHeight() const {
		return m_viewport.getHeight();
	}

	int Camera::getViewportX() const {
		return m_viewport.getX();
	}

	int Camera::getViewportY() const {
		return m_viewport.getY();
	}

	const app::Viewport &Camera::getViewport() const {
		return m_viewport;
	}

}
