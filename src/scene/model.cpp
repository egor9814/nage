//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <GL/glew.h>
#include <nage/scene/model.hpp>
#include <glm/ext.hpp>

namespace nage::scene {

	Model::Model() = default;

	Model::Model(const Model &model) = default;

	Model::Model(Model &&model) noexcept {
		swap(model);
	}

	Model &Model::operator=(const Model &model) = default;

	Model &Model::operator=(Model &&model) noexcept {
		if (this != &model) {
			meshes.clear();
			materials.clear();
			position = glm::vec3(0);
			rotation = glm::vec3(0);
			scale = glm::vec3(1);
			m_transform = glm::mat4(1);
			swap(model);
		}
		return *this;
	}

	Model::~Model() = default;

	void Model::appendMesh(rendering::MaterialId material, const rendering::Mesh &mesh) {
		meshes[material].push_back(mesh);
	}

	void Model::appendMeshes(rendering::MaterialId material, const Model::MeshList &meshList) {
		auto &l = meshes[material];
		l.insert(l.end(), meshList.begin(), meshList.end());
	}

	const glm::mat4x4 &Model::applyTransformation() {
		m_transform = glm::mat4(1);
		m_transform = glm::translate(m_transform, position);
		m_transform = glm::rotate(m_transform, rotation.x, glm::vec3(1, 0, 0));
		m_transform = glm::rotate(m_transform, rotation.y, glm::vec3(0, 1, 0));
		m_transform = glm::rotate(m_transform, rotation.z, glm::vec3(0, 0, 1));
		m_transform = glm::scale(m_transform, scale);
		return m_transform;
	}

	const glm::mat4x4 &Model::getTransform() const {
		return m_transform;
	}

	void Model::draw(Camera *camera, rendering::Shader *shader) const {
		if (shader)
			shader->use();

		for (const auto &it: meshes) {
			if (!materials.contains(it.first)) {
				continue;
			}

			auto material = materials.at(it.first);

			material->bindTextures();
			material->updateUniforms();

			for (const auto &mesh: it.second) {
				material->updateModelMatrix(getTransform() * mesh.getTransform());
				if (camera) {
					material->updateViewMatrix(camera->getViewMatrix());
					material->updateProjectionMatrix(camera->getProjectionMatrix());
				}

				mesh.bind();
				material->enableAttribs();
				mesh.draw();
				material->disableAttribs();
				rendering::Mesh::unbind();
			}

			material->unbindTextures();
		}

		if (shader)
			rendering::Shader::unuse();
	}

	void Model::swap(Model &model) {
		std::swap(materials, model.materials);
		std::swap(meshes, model.meshes);
		std::swap(position, model.position);
		std::swap(rotation, model.rotation);
		std::swap(scale, model.scale);
	}

}
