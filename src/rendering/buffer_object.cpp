//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <nage/rendering/buffer_object.hpp>
#include <memory>
#include "../common.hpp"

namespace nage::rendering {

#pragma region BufferObject::_impl

	BufferObject::_impl::~_impl() {
		BufferObject::unbind(type);
		glDeleteBuffers(1, &id);
	}

#pragma endregion //BufferObject::_impl

#pragma region BufferObject

	BufferObject::BufferObject() : m_impl(nullptr) {}

	BufferObject::BufferObject(const BufferObject &BufferObject) : m_impl(BufferObject.m_impl) {
		if (m_impl)
			m_impl->ref();
	}

	BufferObject::BufferObject(BufferObject &&BufferObject) noexcept: m_impl(nullptr) {
		swap(BufferObject);
	}

	BufferObject &BufferObject::operator=(const BufferObject &bo) {
		if (this != &bo) {
			free();
			m_impl = bo.m_impl;
			if (m_impl)
				m_impl->ref();
		}
		return *this;
	}

	BufferObject &BufferObject::operator=(BufferObject &&bo) noexcept {
		if (this != &bo) {
			free();
			swap(bo);
		}
		return *this;
	}

	BufferObject::~BufferObject() {
		free();
	}

	void BufferObject::free() {
		if (m_impl)
			m_impl->unref();
		m_impl = nullptr;
	}

	void BufferObject::gen(GLenum type) {
		free();

		auto impl = std::make_unique<_impl>();

		glGenBuffers(1, &impl->id);
		impl->type = type;

		m_impl = impl.release();
	}

	bool BufferObject::tryGen(GLenum type, std::string *error) {
		COMMON_TRY(gen(type))
	}

	void BufferObject::bind() const {
		if (!m_impl)
			return;
		glBindBuffer(m_impl->type, m_impl->id);
	}

	void BufferObject::unbind(GLenum type) {
		glBindBuffer(type, 0);
	}

	void BufferObject::data(GLsizeiptr size, const void *data, GLenum usage) const {
		if (!m_impl)
			return;
		glBufferData(m_impl->type, size, data, usage);
	}

	GLenum BufferObject::getType() const {
		if (!m_impl)
			return 0;
		return m_impl->type;
	}

	bool BufferObject::isValid() const {
		return m_impl != nullptr;
	}

	void BufferObject::swap(BufferObject &bo) {
		std::swap(m_impl, bo.m_impl);
	}

#pragma endregion //BufferObject

}