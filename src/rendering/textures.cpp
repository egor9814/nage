//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <GL/glew.h>
#include <nage/rendering/textures.hpp>
#include "../common.hpp"
#include <nage/app/app.hpp>
#include <nage/utils/image.hpp>

namespace nage::rendering {

	Textures::_impl::~_impl() {
		glDeleteTextures(count, ids);
		delete[]ids;
		delete[]ws;
		delete[]hs;
	}

	Textures::Textures() : m_impl(nullptr) {}

	Textures::Textures(GLsizei count) : m_impl(nullptr) {
		create(count);
	}

	Textures::Textures(const Textures &textures) : m_impl(textures.m_impl) {
		if (m_impl)
			m_impl->ref();
	}

	Textures::Textures(Textures &&textures) noexcept: m_impl(nullptr) {
		swap(textures);
	}

	Textures &Textures::operator=(const Textures &textures) {
		if (this != &textures) {
			free();
			m_impl = textures.m_impl;
			if (m_impl)
				m_impl->ref();
		}
		return *this;
	}

	Textures &Textures::operator=(Textures &&textures) noexcept {
		if (this != &textures) {
			free();
			swap(textures);
		}
		return *this;
	}

	Textures::~Textures() {
		free();
	}

	void Textures::free() {
		if (m_impl)
			m_impl->unref();
		m_impl = nullptr;
	}

	void Textures::create(GLsizei count) {
		free();

		if (count < 0)
			throw error("invalid array count: %d", count);

		if (count == 0)
			return;

		auto impl = std::make_unique<_impl>();

		impl->count = count;
		impl->ids = new GLuint[count]{0};
		impl->ws = new GLsizei[count]{0};
		impl->hs = new GLsizei[count]{0};
		glGenTextures(count, impl->ids);

		auto err = glGetError();
		if (err != GL_NO_ERROR)
			throw error("error creating texture buffer! %s", glewGetErrorString(err));

		m_impl = impl.release();
	}

	void Textures::setOptions(GLuint index, const Textures::Options &options) const {
		bind(index);
		setCurrentOptions(options);
		unbind();
	}

	void Textures::setCurrentOptions(const Textures::Options &options) {
		switch (options.filter) {
			default:
			case Options::F_Nearest: {
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				break;
			}
			case Options::F_Linear: {
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				break;
			}
			case Options::F_Trilinear: {
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glGenerateMipmap(GL_TEXTURE_2D);
				break;
			}
		}

		if (options.anisotropyFilter >= 0) {
			if (options.filter == Options::F_Trilinear) {
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, float(options.anisotropyFilter));
			} else {
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 1);
			}
		}

		switch (options.repeat) {
			default:
			case Options::R_Repeat: {
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				break;
			}
			case Options::R_Clamp: {
				GLint wrapMode;
				if (app::Application::getOpenGLVersionMajor() >= 4)
					wrapMode = GL_CLAMP_TO_EDGE;
				else
					wrapMode = GL_CLAMP;
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);
				break;
			}
			case Options::R_ClampToZero: {
				float color[4] = {0.0f, 0.0f, 0.0f, 1.0f};
				glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
				break;
			}
			case Options::R_ClampToZeroAlpha: {
				float color[4] = {0.0f, 0.0f, 0.0f, 0.0f};
				glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
				break;
			}
		}
	}

	void
	Textures::loadFromPixelsRGBA(GLuint index, GLuint *pixels, GLsizei width, GLsizei height, const Options &options) {
		if (!m_impl)
			throw error("texture array not initialized");
		if (index >= m_impl->count)
			throw error("index out of bounds (size: %d, index %d)", m_impl->count, index);

		m_impl->ws[index] = width;
		m_impl->hs[index] = height;

		glBindTexture(GL_TEXTURE_2D, m_impl->ids[index]);

		glTexImage2D(
				GL_TEXTURE_2D,
				/*mipmap level*/ 0,
				GL_RGBA,
				width, height,
				/*border width*/ 0,
				GL_RGBA,
				GL_UNSIGNED_BYTE,
				pixels
		);

		setCurrentOptions(options);

		unbind();

		auto err = glGetError();
		if (err != GL_NO_ERROR)
			throw error("error loading texture from pixels! %s", glewGetErrorString(err));
	}

	bool Textures::tryLoadTextureFromPixelsRGBA(GLuint index, GLuint *pixels, GLsizei width, GLsizei height,
												const Options &options,
												std::string *error) {
		COMMON_TRY(loadFromPixelsRGBA(index, pixels, width, height, options))
	}

	void Textures::loadFromPixelsRGBA(GLuint index, GLuint *pixels, GLsizei width, GLsizei height) {
		loadFromPixelsRGBA(index, pixels, width, height, defaultOptions);
	}

	bool Textures::tryLoadTextureFromPixelsRGBA(GLuint index, GLuint *pixels, GLsizei width, GLsizei height,
												std::string *error) {
		COMMON_TRY(loadFromPixelsRGBA(index, pixels, width, height))
	}

	void Textures::loadFromSurface(GLuint index, SDL_Surface *surface, const Textures::Options &options) {
		utils::Image image(surface, false);
		image = image.convertFormat(SDL_PIXELFORMAT_RGBA32);
		auto s = image.getSurface();
		if (!s)
			throw error("cannot convert image to RGBA8888 pixel format");
		auto pixels = reinterpret_cast<GLuint *>(s->pixels);
		loadFromPixelsRGBA(index, pixels, s->w, s->h, options);
	}

	void Textures::loadFromSurface(GLuint index, SDL_Surface *surface) {
		loadFromSurface(index, surface, defaultOptions);
	}

	bool Textures::tryLoadFromSurface(GLuint index, SDL_Surface *surface, const Textures::Options &options,
									  std::string *error) {
		COMMON_TRY(loadFromSurface(index, surface, options))
	}

	bool Textures::tryLoadFromSurface(GLuint index, SDL_Surface *surface, std::string *error) {
		COMMON_TRY(loadFromSurface(index, surface))
	}

	struct PixelBytes {
		GLuint *data;
		GLsizei size;

		explicit PixelBytes(GLsizei size) : data(new GLuint[size]{0}), size(size) {}

		~PixelBytes() {
			delete[]data;
		}
	};

	void Textures::createFromColor(GLuint index, GLsizei width, GLsizei height,
								   const Options &options,
								   const SDL_Color &color) {
		if (!m_impl)
			throw error("texture array not initialized");
		if (index >= m_impl->count)
			throw error("index out of bounds (size: %d, index %d)", m_impl->count, index);

		PixelBytes pixels{width * height};
		for (GLsizei i = 0; i < pixels.size; i++) {
			auto c = reinterpret_cast<Uint8 *>(&pixels.data[i]);
			c[0] = color.r;
			c[1] = color.g;
			c[2] = color.b;
			c[3] = color.a;
		}
		loadFromPixelsRGBA(index, pixels.data, width, height, options);
	}

	void Textures::createFromColor(GLuint index, GLsizei width, GLsizei height, const SDL_Color &color) {
		createFromColor(index, width, height, defaultOptions, color);
	}

	bool Textures::tryCreateFromColor(GLuint index, GLsizei width, GLsizei height,
									  const Options &options,
									  const SDL_Color &color,
									  std::string *error) {
		COMMON_TRY(createFromColor(index, width, height, options, color))
	}

	bool Textures::tryCreateFromColor(GLuint index, GLsizei width, GLsizei height, const SDL_Color &color,
									  std::string *error) {
		COMMON_TRY(createFromColor(index, width, height, color))
	}

	void Textures::createFromColor(GLuint index, GLsizei width, GLsizei height, const Textures::Options &options,
								   Textures::ColorGenFunction &&f) {
		if (!m_impl)
			throw error("texture array not initialized");
		if (index >= m_impl->count)
			throw error("index out of bounds (size: %d, index %d)", m_impl->count, index);

		PixelBytes pixels{width * height};
		for (GLsizei i = 0; i < pixels.size; i++) {
			auto c = reinterpret_cast<Uint8 *>(&pixels.data[i]);
			auto color = f(width, height, i % width, i / width);
			c[0] = color.r;
			c[1] = color.g;
			c[2] = color.b;
			c[3] = color.a;
		}
		loadFromPixelsRGBA(index, pixels.data, width, height, options);
	}

	void Textures::createFromColor(GLuint index, GLsizei width, GLsizei height, Textures::ColorGenFunction &&f) {
		createFromColor(index, width, height, defaultOptions, std::move(f));
	}

	bool Textures::tryCreateFromColor(GLuint index, GLsizei width, GLsizei height, const Textures::Options &options,
									  Textures::ColorGenFunction &&f, std::string *error) {
		COMMON_TRY(createFromColor(index, width, height, options, std::move(f)))
	}

	bool Textures::tryCreateFromColor(GLuint index, GLsizei width, GLsizei height, Textures::ColorGenFunction &&f,
									  std::string *error) {
		COMMON_TRY(createFromColor(index, width, height, std::move(f)))
	}

	void Textures::bind(GLuint index) const {
		if (!m_impl)
			throw error("texture array not initialized");
		if (index >= m_impl->count)
			throw error("index out of bounds (size: %d, index %d)", m_impl->count, index);
		glBindTexture(GL_TEXTURE_2D, m_impl->ids[index]);
	}

	void Textures::bind(GLuint index, GLuint activeIndex) const {
		if (activeIndex < 0 || activeIndex > 31)
			throw error("active texture index out of bounds(max: 31, min: 0, current: %d)", activeIndex);
		glActiveTexture(GL_TEXTURE0 + activeIndex);
		bind(index);
	}

	void Textures::unbind() {
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void Textures::unbind(GLuint activeIndex) {
		if (activeIndex < 0 || activeIndex > 31)
			throw error("active texture index out of bounds(max: 31, min: 0, current: %d)", activeIndex);
		glActiveTexture(GL_TEXTURE0 + activeIndex);
		unbind();
	}

	bool Textures::isValid() const {
		return m_impl != nullptr;
	}

	GLuint Textures::getId(GLuint index) const {
		if (!m_impl)
			throw error("texture array not initialized");
		if (index >= m_impl->count)
			throw error("index out of bounds (size: %d, index %d)", m_impl->count, index);
		return m_impl->ids[index];
	}

	GLsizei Textures::getWidth(GLuint index) const {
		if (!m_impl)
			throw error("texture array not initialized");
		if (index >= m_impl->count)
			throw error("index out of bounds (size: %d, index %d)", m_impl->count, index);
		return m_impl->ws[index];
	}

	GLsizei Textures::getHeight(GLuint index) const {
		if (!m_impl)
			throw error("texture array not initialized");
		if (index >= m_impl->count)
			throw error("index out of bounds (size: %d, index %d)", m_impl->count, index);
		return m_impl->hs[index];
	}

	GLsizei Textures::getCount() const {
		return m_impl ? m_impl->count : 0;
	}

	void Textures::swap(Textures &textures) {
		std::swap(m_impl, textures.m_impl);
	}

}