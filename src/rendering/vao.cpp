//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <GL/glew.h>
#include <nage/rendering/vao.hpp>
#include "../common.hpp"

namespace nage::rendering {

	VAO::_impl::~_impl() {
		VAO::unbind();
		glDeleteVertexArrays(1, &id);
	}

	VAO::VAO() : m_impl(nullptr) {}

	VAO::VAO(const VAO &vao) : m_impl(vao.m_impl) {
		if (m_impl)
			m_impl->ref();
	}

	VAO::VAO(VAO &&vao) noexcept: m_impl(nullptr) {
		swap(vao);
	}

	VAO &VAO::operator=(const VAO &vao) {
		if (this != &vao) {
			free();
			m_impl = vao.m_impl;
			if (m_impl)
				m_impl->ref();
		}
		return *this;
	}

	VAO &VAO::operator=(VAO &&vao) noexcept {
		if (this != &vao) {
			free();
			swap(vao);
		}
		return *this;
	}

	VAO::~VAO() {
		free();
	}

	void VAO::free() {
		if (m_impl)
			m_impl->unref();
		m_impl = nullptr;
	}

	void VAO::create() {
		free();

		auto impl = std::make_unique<_impl>();

		glCreateVertexArrays(1, &impl->id);

		m_impl = impl.release();
	}

	bool VAO::tryCreate(std::string *error) {
		COMMON_TRY(create())
	}

	void VAO::bind() const {
		if (!m_impl)
			return;
		glBindVertexArray(m_impl->id);
	}

	void VAO::unbind() {
		glBindVertexArray(0);
	}

	bool VAO::isValid() const {
		return m_impl != nullptr;
	}

	void VAO::swap(VAO &vao) {
		std::swap(m_impl, vao.m_impl);
	}

}