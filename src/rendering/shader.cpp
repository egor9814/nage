//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <nage/rendering/shader.hpp>
#include "../common.hpp"
#include <glm/gtc/type_ptr.hpp>

#define METHOD_ERROR(CLASS, METHOD, DESCRIPTION) error(STR(CLASS) "::" STR(METHOD) "(...): " DESCRIPTION)

#define SHADER_IS_NOT_CREATED(METHOD) METHOD_ERROR(Shader, METHOD, "program is not created")

#define ATTRIBUTE_IS_INVALID(METHOD) METHOD_ERROR(Shader::Attrib, METHOD, "location not defined")

#define UNIFORM_IS_INVALID(METHOD) METHOD_ERROR(Shader::Uniform, METHOD, "location not defined")

namespace nage::rendering {

#pragma region ShaderSource::_impl

	ShaderSource::_impl::~_impl() {
		glDeleteShader(id);
	}

#pragma endregion //ShaderSource::_impl

#pragma region ShaderSource

	ShaderSource::ShaderSource() : m_impl(nullptr) {}

	ShaderSource::ShaderSource(const char *source, ShaderSource::Type type) : ShaderSource() {
		load(source, type);
	}

	ShaderSource::ShaderSource(const std::string &source, ShaderSource::Type type) : ShaderSource() {
		load(source, type);
	}

	ShaderSource::ShaderSource(const ShaderSource &s) : m_impl(s.m_impl) {
		if (m_impl)
			m_impl->ref();
	}

	ShaderSource::ShaderSource(ShaderSource &&s) noexcept: m_impl(nullptr) {
		swap(s);
	}

	ShaderSource &ShaderSource::operator=(const ShaderSource &s) {
		if (this != &s) {
			free();
			m_impl = s.m_impl;
			if (m_impl)
				m_impl->ref();
		}
		return *this;
	}

	ShaderSource &ShaderSource::operator=(ShaderSource &&s) noexcept {
		if (this != &s) {
			free();
			swap(s);
		}
		return *this;
	}

	ShaderSource::~ShaderSource() {
		free();
	}

	void ShaderSource::free() {
		if (m_impl)
			m_impl->unref();
		m_impl = nullptr;
	}

	bool ShaderSource::operator==(const ShaderSource &s) const {
		return m_impl == s.m_impl;
	}

	bool ShaderSource::operator!=(const ShaderSource &s) const {
		return m_impl != s.m_impl;
	}

	void ShaderSource::load(const char *source, ShaderSource::Type type) {
		if (m_impl)
			free();

		auto impl = std::make_unique<_impl>();

		impl->id = glCreateShader(type);
		glShaderSource(impl->id, 1, &source, nullptr);
		glCompileShader(impl->id);

		GLint result = GL_FALSE;
		int infoLogLength = 0;
		glGetShaderiv(impl->id, GL_COMPILE_STATUS, &result);
		glGetShaderiv(impl->id, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0 || result == GL_FALSE) {
			std::string err(infoLogLength + 1, 0);
			glGetShaderInfoLog(impl->id, infoLogLength, nullptr, err.data());
			throw std::runtime_error(err);
		}

		m_impl = impl.release();
	}

	bool ShaderSource::tryLoad(const char *source, ShaderSource::Type type, std::string *error) {
		COMMON_TRY(load(source, type))
	}

	void ShaderSource::load(const std::string &source, ShaderSource::Type type) {
		load(source.data(), type);
	}

	bool ShaderSource::tryLoad(const std::string &source, ShaderSource::Type type, std::string *error) {
		COMMON_TRY(load(source, type))
	}

	void ShaderSource::loadFromPath(const std::string &path, ShaderSource::Type type) {
		std::string src;
		if (!readFile(path, src))
			throw error("ShaderSource::loadFromPath(): cannot read shader file %s", path.data());
		load(src, type);
	}

	bool ShaderSource::tryLoadFromPath(const std::string &path, ShaderSource::Type type, std::string *error) {
		COMMON_TRY(loadFromPath(path, type))
	}

	bool ShaderSource::isValid() const {
		return m_impl != nullptr;
	}

	GLuint ShaderSource::getId() const {
		return m_impl ? m_impl->id : -1;
	}

	void ShaderSource::swap(ShaderSource &s) {
		std::swap(m_impl, s.m_impl);
	}

#pragma endregion //ShaderSource

#pragma region Shader::_impl

	Shader::_impl::~_impl() {
		glDeleteProgram(id);
	}

#pragma endregion //Shader::_impl

#pragma region Shader

	Shader::Shader() : m_impl(nullptr) {
		create();
	}

	Shader::Shader(const Shader &shaderProgram) : m_impl(shaderProgram.m_impl) {
		if (m_impl)
			m_impl->ref();
	}

	Shader::Shader(Shader &&shader) noexcept: m_impl(nullptr) {
		swap(shader);
	}

	Shader &Shader::operator=(const Shader &shader) {
		if (this != &shader) {
			free();
			m_impl = shader.m_impl;
			if (m_impl)
				m_impl->ref();
		}
		return *this;
	}

	Shader &Shader::operator=(Shader &&shader) noexcept {
		if (this != &shader) {
			free();
			swap(shader);
		}
		return *this;
	}

	Shader::~Shader() {
		free();
	}

	void Shader::free() {
		if (m_impl)
			m_impl->unref();
		m_impl = nullptr;
	}

	void Shader::create() {
		free();

		auto impl = std::make_unique<_impl>();
		impl->id = glCreateProgram();

		m_impl = impl.release();
	}

	void Shader::attach(const ShaderSource &s, bool takeOwnership) {
		if (!m_impl)
			return;
		if (s.isValid())
			glAttachShader(m_impl->id, s.getId());
		if (takeOwnership)
			m_impl->shaders.push_back(s);
	}

	void Shader::detach(const ShaderSource &s) {
		if (!m_impl)
			return;
		if (s.isValid())
			glDetachShader(m_impl->id, s.getId());
		m_impl->shaders.remove(s);
	}

	void Shader::attach(const char *source, ShaderSource::Type type) {
		attach(ShaderSource(source, type));
	}

	void Shader::attach(const std::string &source, ShaderSource::Type type) {
		attach(ShaderSource(source, type));
	}

	void Shader::attachFile(const std::string &path, ShaderSource::Type type) {
		ShaderSource s;
		s.loadFromPath(path, type);
		attach(s);
	}

	void Shader::link() const {
		if (!m_impl)
			throw SHADER_IS_NOT_CREATED(Shader::link);

		glLinkProgram(m_impl->id);

		GLint result = GL_FALSE;
		int infoLogLength = 0;
		glGetProgramiv(m_impl->id, GL_LINK_STATUS, &result);
		glGetProgramiv(m_impl->id, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0 || result == GL_FALSE) {
			std::string err(infoLogLength + 1, 0);
			glGetProgramInfoLog(m_impl->id, infoLogLength, nullptr, err.data());
			throw std::runtime_error(err);
		}
	}

	bool Shader::tryLink(std::string *error) const {
		COMMON_TRY(link())
	}

	bool Shader::isValid() const {
		return m_impl != nullptr;
	}

	GLuint Shader::getId() const {
		return m_impl ? m_impl->id : -1;
	}

	GLint Shader::getAttribLocation(const char *name) const {
		if (!m_impl)
			return -1;
		return glGetAttribLocation(m_impl->id, name);
	}

	GLint Shader::getUniformLocation(const char *name) const {
		if (!m_impl)
			return -1;
		return glGetUniformLocation(m_impl->id, name);
	}

	Shader::Attrib Shader::getAttrib(GLint location) {
		return Attrib(location);
	}

	Shader::Attrib Shader::getAttrib(const char *name) const {
		return getAttrib(getAttribLocation(name));
	}

	Shader::Uniform Shader::getUniform(GLint location) {
		return Uniform(location);
	}

	Shader::Uniform Shader::getUniform(const char *name) const {
		return getUniform(getUniformLocation(name));
	}

	Shader::Mat4Uniform Shader::getModelMatrix() const {
		return getTypedUniform<glm::mat4>("uModelMatrix");
	}

	Shader::Mat4Uniform Shader::getViewMatrix() const {
		return getTypedUniform<glm::mat4>("uViewMatrix");
	}

	Shader::Mat4Uniform Shader::getProjectionMatrix() const {
		return getTypedUniform<glm::mat4>("uProjectionMatrix");
	}

	void Shader::use() const {
		if (!m_impl)
			throw SHADER_IS_NOT_CREATED(Shader::use);
		glUseProgram(m_impl->id);
	}

	void Shader::unuse() {
		glUseProgram(0);
	}

	void Shader::swap(Shader &shader) {
		std::swap(m_impl, shader.m_impl);
	}

#pragma endregion //Shader

#pragma region Shader::Attrib

	Shader::Attrib &Shader::Attrib::operator=(GLint location) noexcept {
		m_location = location;
		return *this;
	}

	bool Shader::Attrib::isValid() const {
		return m_location != -1;
	}

	GLint Shader::Attrib::getLocation() const {
		return m_location;
	}

	void Shader::Attrib::enableVertexAttribArray() const {
		if (m_location == -1)
			throw ATTRIBUTE_IS_INVALID(enableVertexAttribArray);
		glEnableVertexAttribArray(m_location);
	}

	bool Shader::Attrib::tryEnableVertexAttribArray(std::string *error) const {
		COMMON_TRY(enableVertexAttribArray())
	}

	void Shader::Attrib::disableVertexAttribArray() const {
		if (m_location == -1)
			throw ATTRIBUTE_IS_INVALID(enableVertexAttribArray);
		glDisableVertexAttribArray(m_location);
	}

	bool Shader::Attrib::tryDisableVertexAttribArray(std::string *error) const {
		COMMON_TRY(disableVertexAttribArray())
	}

	void Shader::Attrib::setVertexAttribPointer(GLint size, GLenum type, GLboolean normalized, GLsizei stride,
												const GLvoid *pointer) const {
		if (m_location == -1)
			throw ATTRIBUTE_IS_INVALID(setVertexAttribPointer);
		glVertexAttribPointer(m_location, size, type, normalized, stride, pointer);
	}

	bool Shader::Attrib::trySetVertexAttribPointer(GLint size, GLenum type, GLboolean normalized, GLsizei stride,
												   const GLvoid *pointer, std::string *error) const {
		COMMON_TRY(setVertexAttribPointer(size, type, normalized, stride, pointer))
	}

#pragma endregion //Shader::Attrib

#pragma region Shader::Uniform

	Shader::Uniform &Shader::Uniform::operator=(GLint location) noexcept {
		m_location = location;
		return *this;
	}

	bool Shader::Uniform::isValid() const {
		return m_location != -1;
	}

	GLint Shader::Uniform::getLocation() const {
		return m_location;
	}

	void Shader::Uniform::setVec4f(const glm::vec4 &v) const {
		if (m_location == -1)
			throw UNIFORM_IS_INVALID(setVec4f);
		glUniform4fv(m_location, 1, glm::value_ptr(v));
	}

	void Shader::Uniform::setVec3f(const glm::vec3 &v) const {
		if (m_location == -1)
			throw UNIFORM_IS_INVALID(setVec3f);
		glUniform3fv(m_location, 1, glm::value_ptr(v));
	}

	void Shader::Uniform::setVec2f(const glm::vec2 &v) const {
		if (m_location == -1)
			throw UNIFORM_IS_INVALID(setVec2f);
		glUniform2fv(m_location, 1, glm::value_ptr(v));
	}

	void Shader::Uniform::set1f(float v) const {
		if (m_location == -1)
			throw UNIFORM_IS_INVALID(set1f);
		glUniform1f(m_location, v);
	}

	void Shader::Uniform::set1i(GLint v) const {
		if (m_location == -1)
			throw UNIFORM_IS_INVALID(set1i);
		glUniform1i(m_location, v);
	}

	void Shader::Uniform::set1b(bool v) const {
		if (m_location == -1)
			throw UNIFORM_IS_INVALID(set1b);
		glUniform1i(m_location, v ? 1 : 0);
	}

	void Shader::Uniform::setMat4f(const glm::mat4 &v, bool transpose) const {
		if (m_location == -1)
			throw UNIFORM_IS_INVALID(setMat4f);
		glUniformMatrix4fv(m_location, 1, transpose ? GL_TRUE : GL_FALSE, glm::value_ptr(v));
	}

	void Shader::Uniform::setMat3f(const glm::mat3 &v, bool transpose) const {
		if (m_location == -1)
			throw UNIFORM_IS_INVALID(setMat3f);
		glUniformMatrix3fv(m_location, 1, transpose ? GL_TRUE : GL_FALSE, glm::value_ptr(v));
	}

	void Shader::Uniform::setMat4f(const glm::mat4 &v) const {
		setMat4f(v, false);
	}

	void Shader::Uniform::setMat3f(const glm::mat3 &v) const {
		setMat3f(v, false);
	}

#pragma endregion //Shader::Uniform

}
