//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <GL/glew.h>
#include <nage/rendering/mesh.hpp>
#include <glm/ext.hpp>

namespace nage::rendering {

	void Mesh::recreateBuffers() {
		m_vao.create();
		m_vao.bind();

		m_vbo.gen();
		m_vbo.bind();
		m_vbo.data(GLsizeiptr(m_vertices.size() * sizeof(Vertex)), m_vertices.data());

		if (!m_indices.empty()) {
			m_ebo.gen();
			m_ebo.bind();
			m_ebo.data(GLsizeiptr(m_indices.size() * sizeof(GLuint)), m_indices.data());
		}

		unbind();
	}

	void Mesh::updateBuffers() {
		if (!m_vao.isValid()) {
			return recreateBuffers();
		}

		bind();
		m_vbo.data(GLsizeiptr(m_vertices.size() * sizeof(Vertex)), m_vertices.data());
		if (!m_indices.empty()) {
			if (!m_ebo.isValid()) {
				m_ebo.gen();
				m_ebo.bind();
			}
			m_ebo.data(GLsizeiptr(m_indices.size() * sizeof(GLuint)), m_indices.data());
		}
		unbind();
	}

	Mesh::Mesh() {
		applyTransformation();
	}

	Mesh::Mesh(Vertices vertices, Indices indices, Mode defaultMode) :
			m_vertices(std::move(vertices)), m_indices(std::move(indices)), defaultMode(defaultMode) {
		recreateBuffers();
		applyTransformation();
	}

	Mesh::Mesh(const Mesh &mesh) = default;

	Mesh::Mesh(Mesh &&mesh) noexcept {
		swap(mesh);
	}

	Mesh &Mesh::operator=(const Mesh &mesh) {
		if (this != &mesh) {
			m_vertices = mesh.m_vertices;
			m_indices = mesh.m_indices;
			m_transform = mesh.m_transform;
			position = mesh.position;
			rotation = mesh.rotation;
			scale = mesh.scale;
			defaultMode = mesh.defaultMode;
			updateBuffers();
		}
		return *this;
	}

	Mesh &Mesh::operator=(Mesh &&mesh) noexcept {
		if (this != &mesh) {
			m_vertices.clear();
			m_indices.clear();
			m_vao.free();
			m_vbo.free();
			m_ebo.free();
			m_transform = glm::mat4x4(0);
			position = glm::vec3(0);
			rotation = glm::vec3(0);
			scale = glm::vec3(0);
			defaultMode = M_Points;
			swap(mesh);
		}
		return *this;
	}

	void Mesh::setData(const Vertices &vertices, const Indices &indices) {
		m_vertices = vertices;
		m_indices = indices;
		updateBuffers();
	}

	const Mesh::Vertices &Mesh::getVertices() const {
		return m_vertices;
	}

	const Mesh::Indices &Mesh::getIndices() const {
		return m_indices;
	}

	void Mesh::bind() const {
		m_vao.bind();
		m_vbo.bind();
		m_ebo.bind();
	}

	void Mesh::unbind() {
		VAO::unbind();
		VBO::unbind();
		EBO::unbind();
	}

	const glm::mat4x4 &Mesh::applyTransformation() {
		m_transform = glm::mat4(1);
		m_transform = glm::translate(m_transform, origin);
		m_transform = glm::rotate(m_transform, rotation.x, glm::vec3(1, 0, 0));
		m_transform = glm::rotate(m_transform, rotation.y, glm::vec3(0, 1, 0));
		m_transform = glm::rotate(m_transform, rotation.z, glm::vec3(0, 0, 1));
		m_transform = glm::translate(m_transform, position - origin);
		m_transform = glm::scale(m_transform, scale);
		return m_transform;
	}

	const glm::mat4x4 &Mesh::getTransform() const {
		return m_transform;
	}

	void Mesh::setTransform(const glm::mat4x4 &transform) {
		m_transform = transform;
	}

	void Mesh::draw(Mode mode) const {
		if (m_indices.empty())
			glDrawArrays(mode, 0, GLsizei(m_vertices.size()));
		else
			glDrawElements(mode, GLsizei(m_indices.size()), GL_UNSIGNED_INT, nullptr);
	}

	void Mesh::draw() const {
		draw(defaultMode);
	}

	void Mesh::swap(Mesh &mesh) {
		std::swap(m_vertices, mesh.m_vertices);
		std::swap(m_indices, mesh.m_indices);
		std::swap(m_vao, mesh.m_vao);
		std::swap(m_vbo, mesh.m_vbo);
		std::swap(m_ebo, mesh.m_ebo);
		std::swap(m_transform, mesh.m_transform);
		std::swap(position, mesh.position);
		std::swap(rotation, mesh.rotation);
		std::swap(scale, mesh.scale);
		std::swap(defaultMode, mesh.defaultMode);
		updateBuffers();
	}

}