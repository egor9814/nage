//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#define _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING

#include <GL/glew.h>
#include "common.hpp"
#include <fstream>
#include <sstream>
#include <locale>
#include <codecvt>
#include <algorithm>

namespace nage {

	const char *glGetErrorString(unsigned int code) {
		switch (code) {
			case GL_INVALID_ENUM:
				return "Invalid enum";
			case GL_INVALID_OPERATION:
				return "Invalid operation";
			case GL_STACK_OVERFLOW:
				return "Stack overflow";
			case GL_STACK_UNDERFLOW:
				return "Stack Underflow";
			case GL_OUT_OF_MEMORY:
				return "Out of memory";
			default:
				return "Unknown error";
		}
	}

	enum class Encoding {
		ASCII = 0,
		UTF8,
		UTF16LE,
		UTF16BE,
	};

	bool readFile(const std::string &path, std::string &result) {
		/// https://gist.github.com/VeryCrazyDog/c20b2cb83896e9975d22
		std::ifstream ifs(path.data(), std::ios::binary);
		std::stringstream ss;
		auto encoding = Encoding::ASCII;

		if (!ifs.is_open()) {
			// Unable to read file
			result.clear();
			return false;
		} else if (ifs.eof()) {
			result.clear();
		} else {
			int ch1 = ifs.get();
			int ch2 = ifs.get();
			if (ch1 == 0xff && ch2 == 0xfe) {
				// The file contains UTF-16LE BOM
				encoding = Encoding::UTF16LE;
			} else if (ch1 == 0xfe && ch2 == 0xff) {
				// The file contains UTF-16BE BOM
				encoding = Encoding::UTF16BE;
			} else {
				int ch3 = ifs.get();
				if (ch1 == 0xef && ch2 == 0xbb && ch3 == 0xbf) {
					// The file contains UTF-8 BOM
					encoding = Encoding::UTF8;
				} else {
					// The file does not have BOM
					encoding = Encoding::ASCII;
					ifs.seekg(0);
				}
			}
		}
		ss << ifs.rdbuf() << '\0';
		switch (encoding) {
			case Encoding::UTF16LE: {
				std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> utfconv;
				result = utfconv.to_bytes(std::wstring((wchar_t *) ss.str().c_str()));
				break;
			}
			case Encoding::UTF16BE: {
				std::string src = ss.str();
				std::string dst = src;
#ifdef WIN32
				// Using Windows API
				_swab(&src[0u], &dst[0u], int(src.size() + 1));
#else
				// WARNING! not tested code {
			auto _src = &src[0u];
			auto _dst = &dst[0u];
			auto _len = src.size() + 1;
			for (int i = 0; i < _len; i+=2){
				_dst[i] = _src[i+1];
				_dst[i+1] = _src[i];
			}
			// }
#error not tested code
#endif
				std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> utfconv;
				result = utfconv.to_bytes(std::wstring((wchar_t *) dst.c_str()));
				break;
			}
			default: {
				result = ss.str();
			}
		}
		return true;
	}

	std::string dirname(const std::string &path) {
		auto index = path.find_last_of("\\/");
		if (index == std::string::npos)
			return {};
		return path.substr(0, index);
	}

	bool parseInt(const std::string &s, int &out) {
		std::stringstream ss{s};
		int i;
		ss >> i;
		if (ss.good()) {
			out = i;
			return true;
		}
		return false;
	}

	std::string toLowerCase(const std::string &s) {
		/// https://stackoverflow.com/questions/313970/how-to-convert-an-instance-of-stdstring-to-lower-case
		std::string d{s};
		std::transform(d.begin(), d.end(), d.begin(),
					   [](unsigned char c) { return std::tolower(c); });
		return d;
	}

	bool parseBool(const std::string &s, bool &out) {
		auto d = toLowerCase(s);
		if (d == "true" || d == "1") {
			out = true;
		} else if (d == "false" || d == "0") {
			out = false;
		} else {
			return false;
		}
		return true;
	}

	std::atomic_uint_fast64_t &glErrorsCount() {
		static std::atomic_uint_fast64_t count{0};
		return count;
	}

	void clearGL() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	}
}