//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef NAGE_COMMON_HPP
#define NAGE_COMMON_HPP

#include <stdexcept>
#include <string>
#include <memory>
#include <vector>
#include <cstdarg>

#ifndef STR2
#define STR2(X) #X
#endif
#ifndef STR
#define STR(X) STR2(X)
#endif

#ifndef CAT2
#define CAT2(X, Y) X ## Y
#endif
#ifndef CAT
#define CAT(X, Y) CAT2(X, Y)
#endif

namespace nage {

	namespace common_detail {

		inline std::string format_impl(const char *fmt, ...) {
			// https://habr.com/ru/post/318962/
			va_list args;
					va_start(args, fmt);
			std::vector<char> v(1024);
			while (true) {
				va_list args2;
				va_copy(args2, args);
				int res = vsnprintf(v.data(), v.size(), fmt, args2);
				if ((res >= 0) && (res < static_cast<int>(v.size()))) {
							va_end(args);
							va_end(args2);
					return {v.data()};
				}
				size_t size;
				if (res < 0)
					size = v.size() * 2;
				else
					size = static_cast<size_t>(res) + 1;
				v.clear();
				v.resize(size);
						va_end(args2);
			}
		}

	}

	template<typename ... Args>
	inline std::string format(const std::string &message, Args &&...args) {
		return common_detail::format_impl(message.data(), std::forward<Args>(args)...);
	}

	template<typename ... Args>
	inline std::runtime_error error(const std::string &message, Args &&...args) {
		return std::runtime_error(format(message, std::forward<Args>(args)...));
	}

	const char *glGetErrorString(unsigned int code);

	bool readFile(const std::string &path, std::string &result);

	std::string dirname(const std::string &path);

	bool parseInt(const std::string &s, int &out);

	std::string toLowerCase(const std::string &s);

	bool parseBool(const std::string &s, bool &out);

	std::atomic_uint_fast64_t &glErrorsCount();

	void clearGL();
}

#define COMMON_TRY(ACTION) \
try {\
    (ACTION);\
    return true;\
} catch (const std::exception &err) {\
    if (error) *error = err.what();\
} catch (...) {\
    if (error) *error = "unknown exception";\
}\
return false;

#endif //NAGE_COMMON_HPP
