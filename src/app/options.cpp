//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <nage/app/options.hpp>
#include <iostream>
#include <sstream>
#include "../common.hpp"

namespace nage::app {

	struct IParseOutput {
		virtual ~IParseOutput() = default;

		virtual void message(const std::string &s) {};

		virtual void warning(const std::string &s) {};

		virtual void error(const std::string &s) {};
	};

	struct StreamParseOutput : IParseOutput {
		std::ostream *out;

		explicit StreamParseOutput(std::ostream *out) : out(out) {}

		void message(const std::string &s) override {
			(*out) << s << std::endl;
		}

		void warning(const std::string &s) override {
			(*out) << "warning: " << s << std::endl;
		}

		void error(const std::string &s) override {
			(*out) << "error: " << s << std::endl;
		}
	};

	struct ListParseOutput : IParseOutput {
		std::list<std::string> *out;

		explicit ListParseOutput(std::list<std::string> *out) : out(out) {}

		void message(const std::string &s) override {
			if (out)
				out->push_back(s);
		}

		void warning(const std::string &s) override {
			if (out)
				out->push_back("warning: " + s);
		}

		void error(const std::string &s) override {
			if (out)
				out->push_back("error: " + s);
		}
	};

	std::shared_ptr<IParseOutput>
	createParseOutput(Options::ParseMode parseMode, std::list<std::string> *unparsedArgs) {
		switch (parseMode) {
			default:
			case Options::PM_InvalidArgIgnore:
				return std::make_shared<IParseOutput>();
			case Options::PM_InvalidArgToStdOut:
				return std::make_shared<StreamParseOutput>(&std::cout);
			case Options::PM_InvalidArgToStdErr:
				return std::make_shared<StreamParseOutput>(&std::cerr);
			case Options::PM_InvalidArgToUnparsed:
				return std::make_shared<ListParseOutput>(unparsedArgs);
		}
	}

	bool parseOpenGLVersion(const std::string &s, int &major, int &minor) {
		std::stringstream ss{s};
		int mj, mn;
		char c;
		if (ss >> mj >> c >> mn && c == '.') {
			minor = mn;
			major = mj;
			return true;
		}
		return false;
	}

	void Options::parse(int &argc, char **&argv, Options::ParseMode parseMode, std::list<std::string> *unparsedArgs) {
		auto out = createParseOutput(parseMode, unparsedArgs);
		std::stringstream ss;

		for (int i = 1; i < argc; i++) {
			std::string arg{argv[i]};
			if (arg.starts_with("-vulkan")) {
				out->warning("Vulkan API not supported yet");
			}
			if (arg.starts_with("-opengl=")) {
				if (!parseOpenGLVersion(arg.substr(8), glVersionMajor, glVersionMinor)) {
					ss.clear();
					ss.str("");
					ss << "invalid OpenGL version, using by default "
						  "OpenGL " << glVersionMajor << "." << glVersionMinor;
					out->warning(ss.str());
				}
			} else if (arg.starts_with("-title=")) {
				title = arg.substr(7);
			} else if (arg.starts_with("-width=")) {
				if (!parseInt(arg.substr(7), defaultWidth)) {
					ss.clear();
					ss.str("");
					ss << "invalid width, using by default " << defaultWidth;
					out->warning(ss.str());
				}
			} else if (arg.starts_with("-height=")) {
				if (!parseInt(arg.substr(8), defaultHeight)) {
					ss.clear();
					ss.str("");
					ss << "invalid height, using by default " << defaultHeight;
					out->warning(ss.str());
				}
			} else if (arg.starts_with("-x=")) {
				if (arg == "-x=none" || arg == "-x=undefined") {
					defaultPositionX = SDL_WINDOWPOS_UNDEFINED;
				} else if (arg == "-x=c" || arg == "-x=center" || arg == "-x=centered") {
					defaultPositionX = SDL_WINDOWPOS_CENTERED;
				} else if (!parseInt(arg.substr(3), defaultPositionX)) {
					ss.clear();
					ss.str("");
					ss << "invalid position x, using by default ";
					if (SDL_WINDOWPOS_ISUNDEFINED(defaultPositionX))
						ss << "UNDEFINED";
					else if (SDL_WINDOWPOS_ISCENTERED(defaultPositionX))
						ss << "CENTERED";
					else
						ss << defaultPositionX;
					out->warning(ss.str());
				}
			} else if (arg.starts_with("-y=")) {
				ss.clear();
				ss.str("");
				if (arg == "-y=none" || arg == "-y=undefined") {
					defaultPositionY = SDL_WINDOWPOS_UNDEFINED;
				} else if (arg == "-y=c" || arg == "-y=center" || arg == "-y=centered") {
					defaultPositionY = SDL_WINDOWPOS_CENTERED;
				} else if (!parseInt(arg.substr(3), defaultPositionY)) {
					ss << "invalid position y, using by default ";
					if (SDL_WINDOWPOS_ISUNDEFINED(defaultPositionY))
						ss << "UNDEFINED";
					else if (SDL_WINDOWPOS_ISCENTERED(defaultPositionY))
						ss << "CENTERED";
					else
						ss << defaultPositionY;
					out->warning(ss.str());
				}
			} else if (arg.starts_with("-gl-red-size=")) {
				if (!parseInt(arg.substr(13), glRedSize)) {
					ss.clear();
					ss.str("");
					ss << "invalid GL red size, using by default " << glRedSize;
					out->warning(ss.str());
				}
			} else if (arg.starts_with("-gl-green-size=")) {
				if (!parseInt(arg.substr(15), glGreenSize)) {
					ss.clear();
					ss.str("");
					ss << "invalid GL green size, using by default " << glGreenSize;
					out->warning(ss.str());
				}
			} else if (arg.starts_with("-gl-blue-size=")) {
				if (!parseInt(arg.substr(14), glBlueSize)) {
					ss.clear();
					ss.str("");
					ss << "invalid GL blue size, using by default " << glBlueSize;
					out->warning(ss.str());
				}
			} else if (arg.starts_with("-gl-alpha-size=")) {
				if (!parseInt(arg.substr(15), glAlphaSize)) {
					ss.clear();
					ss.str("");
					ss << "invalid GL alpha size, using by default " << glAlphaSize;
					out->warning(ss.str());
				}
			} else if (arg.starts_with("-gl-depth-size=")) {
				if (!parseInt(arg.substr(15), glDepthSize)) {
					ss.clear();
					ss.str("");
					ss << "invalid GL depth size, using by default " << glDepthSize;
					out->warning(ss.str());
				}
			} else if (arg == "-gl-doublebuffer") {
				glDoubleBuffer = true;
			} else if (arg.starts_with("-gl-doublebuffer=")) {
				if (!parseBool(arg.substr(17), glDoubleBuffer)) {
					ss.clear();
					ss.str("");
					ss << "invalid double buffer flag, using by default " << glDoubleBuffer;
					out->warning(ss.str());
				}
			} else if (arg == "-gl-no-doublebuffer") {
				glDoubleBuffer = false;
			} else if (arg.starts_with("-gl-msb=")) {
				if (!parseInt(arg.substr(8), glMultiSampleBuffers)) {
					ss.clear();
					ss.str("");
					ss << "invalid GL multisample buffers count, using by default " << glMultiSampleBuffers;
					out->warning(ss.str());
				}
			} else if (arg.starts_with("-gl-mss=")) {
				if (!parseInt(arg.substr(8), glMultiSampleSamples)) {
					ss.clear();
					ss.str("");
					ss << "invalid GL multisample samples count, using by default " << glMultiSampleSamples;
					out->warning(ss.str());
				}
			} else if (arg == "-gl-av" || arg == "-gl-accelerate-visual") {
				glAcceleratedVisual = true;
			} else if (arg.starts_with("-gl-accelerate-visual=")) {
				if (!parseBool(arg.substr(22), glAcceleratedVisual)) {
					ss.clear();
					ss.str("");
					ss << "invalid accelerate visual flag, using by default " << glAcceleratedVisual;
					out->warning(ss.str());
				}
			} else if (arg == "-gl-no-av" || arg == "-gl-no-accelerate-visual") {
				glAcceleratedVisual = false;
			} else if (arg == "-gl-debug") {
				glDebug = true;
			} else if (arg.starts_with("-gl-debug=")) {
				if (!parseBool(arg.substr(10), glDebug)) {
					ss.clear();
					ss.str("");
					ss << "invalid debug flag, using by default " << glDebug;
					out->warning(ss.str());
				}
			} else if (arg == "-gl-no-debug") {
				glDebug = false;
			} else if (arg == "-allow-hdpi") {
				allowHighDPI = true;
			} else if (arg.starts_with("-allow-hdpi=")) {
				if (!parseBool(arg.substr(12), allowHighDPI)) {
					ss.clear();
					ss.str("");
					ss << "invalid allow high DPI flag, using by default " << allowHighDPI;
					out->warning(ss.str());
				}
			} else if (arg == "-not-allow-hdpi") {
				allowHighDPI = false;
			} else if (arg == "-allow-resizable") {
				allowResizable = true;
			} else if (arg.starts_with("-allow-resizable=")) {
				if (!parseBool(arg.substr(12), allowResizable)) {
					ss.clear();
					ss.str("");
					ss << "invalid allow resizable flag, using by default " << allowResizable;
					out->warning(ss.str());
				}
			} else if (arg == "-not-allow-resizable") {
				allowResizable = false;
			} else if (unparsedArgs) {
				unparsedArgs->push_back(arg);
			}
		}
	}

}
