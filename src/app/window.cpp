//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <GL/glew.h>
#include <nage/app/window.hpp>
#include "../common.hpp"

namespace nage::app {

	void GLAPIENTRY
	GLMessageCallback(GLenum source,
					  GLenum type,
					  GLuint id,
					  GLenum severity,
					  GLsizei length,
					  const GLchar *message,
					  const void *userParam) {
		fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
				(type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
				type, severity, message);
		if (type == GL_DEBUG_TYPE_ERROR)
			glErrorsCount()++;
	}

	void initGL(int w, int h) {
		glViewport(0, 0, w, h);

		// Initialize clear color
		glClearColor(0.f, 0.f, 0.f, 1.f);

		// Enable depth test
		glEnable(GL_DEPTH_TEST);

		// Enable face culling
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glFrontFace(GL_CCW);

		// Enable blending
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		// Set antialiasing/multisampling
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
		glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
		glDisable(GL_LINE_SMOOTH);
		glDisable(GL_POLYGON_SMOOTH);
		glDisable(GL_MULTISAMPLE);

		// Check for error
		auto err = glGetError();
		if (err != GL_NO_ERROR) {
			throw error("error initializing OpenGL! '%s' (code: %d)", glGetErrorString(err), err);
		}
	}

	bool findDisplay(int posX, int posY, SDL_Rect *bounds, SDL_DisplayMode *displayMode) {
		if (!bounds)
			return false;
		auto displayCount = SDL_GetNumVideoDisplays();
		for (int i = 0; i < displayCount; i++) {
			SDL_GetDisplayBounds(i, bounds);
			auto left = bounds->x;
			auto right = bounds->x + bounds->w;
			auto top = bounds->y;
			auto bottom = bounds->y + bounds->h;
			if (left <= posX && posX <= right &&
				top <= posY && posY <= bottom) {
				if (displayMode)
					SDL_GetCurrentDisplayMode(i, displayMode);
				return true;
			}
		}
		return false;
	}

	Window::Window(SDL_Window *window, bool debug) : m_window(window) {
		m_glContext = SDL_GL_CreateContext(m_window);

		glewExperimental = true;
		auto err = glewInit();
		if (err != GLEW_OK) {
			auto str = glewGetErrorString(err);
			throw error("GLEW error: %s", reinterpret_cast<const char *>(str));
		}

		if (debug) {
			glEnable(GL_DEBUG_OUTPUT);
			glDebugMessageCallback(GLMessageCallback, nullptr);
		}

		SDL_GetWindowPosition(m_window, &m_posX, &m_posY);

		SDL_GetWindowSize(m_window, &m_viewport.right, &m_viewport.bottom);

		initGL(m_viewport.right, m_viewport.bottom);

		updateView();
	}

	Window::~Window() {
		SDL_GL_DeleteContext(m_glContext);
		SDL_DestroyWindow(m_window);
	}

	void Window::setFullscreen(WindowFullscreenMode mode) {
		if (mode == m_fullscreen)
			return;

		if (mode != WindowFullscreenMode::Windowed) {
			SDL_GetWindowPosition(m_window, &m_posX, &m_posY);
			SDL_GetWindowDisplayMode(m_window, &m_windowDisplayMode);

			SDL_Rect bounds;
			SDL_DisplayMode displayMode;
			if (!findDisplay(m_posX, m_posY, &bounds, &displayMode))
				return;

			SDL_SetWindowDisplayMode(m_window, &displayMode);
			clearGL();
			SDL_SetWindowFullscreen(m_window, Uint32(mode));
		} else {
			SDL_SetWindowFullscreen(m_window, SDL_FALSE);
			SDL_SetWindowDisplayMode(m_window, &m_windowDisplayMode);
			clearGL();
		}
		m_fullscreen = mode;
	}

	WindowFullscreenMode Window::getFullscreenMode() const {
		return m_fullscreen;
	}

	int Window::getWidth() const {
		return m_viewport.right;
	}

	int Window::getHeight() const {
		return m_viewport.bottom;
	}

	void Window::setSize(int width, int height) {
		SDL_SetWindowSize(m_window, width, height);
	}

	void Window::setIcon(const SDL_Surface &surface) {
		SDL_SetWindowIcon(m_window, const_cast<SDL_Surface *>(&surface));
	}

	const Viewport &Window::getViewport() const {
		return m_viewport;
	}

	void Window::swapWindow() const {
		SDL_GL_SwapWindow(m_window);
	}

	void Window::onWindowEvent(const SDL_WindowEvent &event) {
		switch (event.event) {
			default:
				break;
			case SDL_WINDOWEVENT_SIZE_CHANGED:
				m_viewport.right = event.data1;
				m_viewport.bottom = event.data2;
				updateView();
				swapWindow();
				return;
			case SDL_WINDOWEVENT_EXPOSED:
				swapWindow();
				return;
		}
	}

	void Window::updateView() {
		SDL_GL_GetDrawableSize(m_window, &m_viewport.right, &m_viewport.bottom);
		glViewport(0, 0, m_viewport.right, m_viewport.bottom);
	}
}