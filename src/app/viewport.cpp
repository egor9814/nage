//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <GL/glew.h>
#include <nage/app/viewport.hpp>

namespace nage::app {

	void Viewport::setWidth(int v) {
		right = left + v;
	}

	int Viewport::getWidth() const {
		return right - left;
	}

	void Viewport::setHeight(int v) {
		bottom = top + v;
	}

	int Viewport::getHeight() const {
		return bottom - top;
	}

	void Viewport::translate(int dx, int dy) {
		left += dx;
		top += dy;
		right += dx;
		bottom += dy;
	}

	void Viewport::setX(int v) {
		right = v + getWidth();
		left = v;
	}

	int Viewport::getX() const {
		return left;
	}

	void Viewport::setY(int v) {
		bottom = v + getHeight();
		top = v;
	}

	int Viewport::getY() const {
		return top;
	}

	void Viewport::apply() const {
		glViewport(getX(), getY(), getWidth(), getHeight());
	}

}
