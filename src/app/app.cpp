//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <GL/glew.h>
#include <nage/app/app.hpp>
#include <nage/app/window.hpp>
#include <nage/scene/scene.hpp>
#include <nage/input/input.hpp>
#include <mutex>
#include <iostream>
#include "../common.hpp"
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>

#ifdef WIN32

#include <windows.h>

namespace windows {

	void hideConsole() {
		auto hwnd = GetConsoleWindow();
		if (!hwnd)
			return;
		ShowWindow(hwnd, SW_HIDE);
	}

}

#endif

namespace nage::app {

	struct ApplicationInstance {

		using lock_t = std::lock_guard<std::mutex>;

		static Application *get() {
			auto &i = getInstance();
			lock_t lock{i.mtx};
			return i.instance;
		}

		static void set(Application *app) {
			auto &i = getInstance();
			lock_t lock{i.mtx};
			i.instance = app;
		}

	private:
		static ApplicationInstance &getInstance() {
			static ApplicationInstance i;
			return i;
		}

		ApplicationInstance() = default;

		~ApplicationInstance() {
			mtx.lock();
			if (instance) {
				mtx.unlock();
				delete instance;
				mtx.lock();
			}
			if (instance) {
				std::cerr << "cannot remove application instance" << std::endl;
			}
			mtx.unlock();
		}

		Application *instance{nullptr};
		std::mutex mtx;
	};

	Application::Application(const Options &options) {
		if (ApplicationInstance::get())
			throw error("Application instance already exists");

#ifdef WIN32
		windows::hideConsole();
#endif

		if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
			throw error("SDL could not initialize! SDL_Error: %s", SDL_GetError());

		auto imgInitFlags = IMG_InitFlags(
				0
#ifdef _NAGE_CONFIG_SDL_IMAGE_SUPPORT_PNG
				| IMG_INIT_PNG
#endif
#ifdef _NAGE_CONFIG_SDL_IMAGE_SUPPORT_JPEG
				| IMG_INIT_JPG
#endif
#ifdef _NAGE_CONFIG_SDL_IMAGE_SUPPORT_TIFF
				| IMG_INIT_TIF
#endif
#ifdef _NAGE_CONFIG_SDL_IMAGE_SUPPORT_WEBP
				| IMG_INIT_WEBP
#endif
		);
		if (imgInitFlags != 0 && !(IMG_Init(imgInitFlags) & imgInitFlags))
			throw error("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());

		if (TTF_Init() == -1)
			throw error("SDL_ttf could not initialize! SDL_ttf Error: %s", TTF_GetError());

		int contextFlags = 0;

		SDL_GL_SetAttribute(SDL_GL_RED_SIZE, options.glRedSize);
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, options.glGreenSize);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, options.glBlueSize);
		SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, options.glAlphaSize);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, options.glDepthSize);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, options.glDoubleBuffer ? 1 : 0);

		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, options.glVersionMajor);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, options.glVersionMinor);
		if (options.glVersionMajor >= 3)
			contextFlags |= SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG;
		if (options.glVersionMajor >= 3 && options.glVersionMinor >= 2)
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, options.glMultiSampleBuffers);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, options.glMultiSampleSamples);

		SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, options.glAcceleratedVisual ? 1 : 0);

		if (options.glDebug)
			contextFlags |= SDL_GL_CONTEXT_DEBUG_FLAG;
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, contextFlags);

		Uint32 windowFlags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL;
		if (options.allowHighDPI)
			windowFlags |= SDL_WINDOW_ALLOW_HIGHDPI;
		if (options.allowResizable)
			windowFlags |= SDL_WINDOW_RESIZABLE;
		auto nativeWindow = SDL_CreateWindow(
				options.title.data(),
				options.defaultPositionX, options.defaultPositionY,
				options.defaultWidth, options.defaultHeight,
				windowFlags
		);
		if (!nativeWindow)
			throw error("Window could not be created! SDL_Error: %s", SDL_GetError());
		m_window.reset(new Window(nativeWindow, options.glDebug));

		ApplicationInstance::set(this);
	}

	Application::~Application() {
		TTF_Quit();
		IMG_Quit();
		Mix_Quit();
		SDL_Quit();

		ApplicationInstance::set(nullptr);
	}

	bool Application::setSwapInterval(Application::SwapInterval interval) {
		if (!ApplicationInstance::get())
			return false;
		if (SDL_GL_SetSwapInterval(interval) < 0) {
			std::cerr << format("warning: Unable to set swap interval to %d! SDL Error: %s\n", interval, SDL_GetError())
					  << std::endl;
			return false;
		}
		return true;
	}

	void Application::quit() {
		auto app = ApplicationInstance::get();
		if (app)
			app->m_run.store(false);
	}

	int Application::start() {
		if (m_run.load())
			return -2;
		mainLoop();
		return glErrorsCount() ? -3 : 0;
	}

	void Application::restart() {
		auto app = ApplicationInstance::get();
		if (!app)
			return;
		if (app->m_restart.load())
			return;
		if (!app->m_run)
			return;
		app->m_restart = true;
		app->m_run = false;
	}

	void Application::setAliasMode(Application::AliasMode value) {
		auto app = ApplicationInstance::get();
		if (app)
			app->m_aliasMode = value;
	}

	void Application::setScene(scene::Scene *scene) {
		auto app = ApplicationInstance::get();
		if (app)
			app->m_scene = scene;
	}

	scene::Scene *Application::getScene() {
		auto app = ApplicationInstance::get();
		return app ? app->m_scene : nullptr;
	}

	Application::AliasMode Application::getAliasMode() {
		auto app = ApplicationInstance::get();
		if (app)
			return app->m_aliasMode;
		return AM_None;
	}

	int Application::getOpenGLVersionMajor() {
		int result{0};
		auto code = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &result);
		return code == 0 ? result : code;
	}

	int Application::getOpenGLVersionMinor() {
		int result{0};
		auto code = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &result);
		return code == 0 ? result : code;
	}

	Window *Application::getWindow() {
		auto app = ApplicationInstance::get();
		if (app)
			return app->m_window.get();
		return nullptr;
	}

	void Application::onEvent(const SDL_Event &event) {
		if (event.type == SDL_CONTROLLERDEVICEADDED) {
			input::Input::get().addGamepad(event.cdevice);
		}
		if (event.type == SDL_CONTROLLERDEVICEREMOVED) {
			input::Input::get().removeGamepad(event.cdevice);
		}
		if (m_scene && m_scene->filterEvent(event))
			return;
		switch (event.type) {
			default:
				break;
			case SDL_WINDOWEVENT:
				m_window->onWindowEvent(event.window);
				return;
		}
	}

	void Application::onUpdate(float deltaTime) {
		input::Input::poll();
		if (m_scene) {
			m_scene->update(deltaTime);
		}
	}

	void Application::onRender() const {
		auto aliasMode = m_aliasMode;
		if (aliasMode != AM_None) {
			//Start alias mode
			switch (aliasMode) {
				default:
					break;

				case AM_Aliased:
					glDisable(GL_LINE_SMOOTH);
					glDisable(GL_POLYGON_SMOOTH);
					glDisable(GL_MULTISAMPLE);
					break;

				case AM_AntiAliased:
					glEnable(GL_LINE_SMOOTH);
					glEnable(GL_POLYGON_SMOOTH);
					glDisable(GL_MULTISAMPLE);
					break;

				case AM_Multisample:
					glDisable(GL_LINE_SMOOTH);
					glDisable(GL_POLYGON_SMOOTH);
					glEnable(GL_MULTISAMPLE);
					break;
			}
		}

		if (m_scene) {
			m_scene->render(nullptr);
		}

		if (aliasMode != AM_None) {
			//End alias mode
			switch (aliasMode) {
				default:
					break;

				case AM_AntiAliased:
					glDisable(GL_LINE_SMOOTH);
					glDisable(GL_POLYGON_SMOOTH);
					break;

				case AM_Multisample:
					glDisable(GL_MULTISAMPLE);
					break;
			}
		}
	}

	void Application::swapWindow() const {
		m_window->swapWindow();
	}

	void Application::render() const {
		clearGL();
		onRender();
		swapWindow();
		glFlush();
	}

	void Application::loopStep(Uint64 &simulationTime, SDL_Event &e) {
		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) {
				quit();
				return;
			}
			onEvent(e);
		}
		auto realTime = SDL_GetTicks64();
		while (simulationTime < realTime) {
			simulationTime += 16;
			onUpdate(0.016f);
		}
		render();
	}

	void Application::mainLoop() {
		if (m_run.load())
			return;

		m_run.store(true);
		SDL_Event e;
		Uint64 simulationTime{SDL_GetTicks64()};
		while (true) {
			// TODO: async update
			while (m_run.load()) {
				loopStep(simulationTime, e);
			}
			if (m_restart.load()) {
				m_run = true;
				m_restart = false;
			} else {
				break;
			}
		}
	}

}