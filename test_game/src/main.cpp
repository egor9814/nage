//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <iostream>
#include <nage/all>
#include <SDL.h>
#include <glm/ext.hpp>

using namespace nage;
using namespace nage::input;

struct InputEventListenerImpl : public IInputEventListener {
	void onGamepadAttached() override {
		auto count = Input::getGamepadsCount();
		std::cout << "debug: gamepad attached, count = " << count << std::endl;
		for (int i = 0; i < count; i++) {
			std::cout << "debug:   gamepad {" << i << "}: " << Input::getGamepadName(i) << std::endl;
		}
	}

	void onGamepadDetached() override {
		std::cout << "debug: gamepad detached, count = " << Input::getGamepadsCount() << std::endl;
	}
};

struct CubeModel {
	struct CubeMaterial : rendering::Material {
		rendering::Shader::Attrib pos, tex, norm;
		rendering::Shader::Mat4Uniform modelMatrix, projectionMatrix, viewMatrix;
		rendering::Shader::Sampler2DUniform diffuseTex1, diffuseTex2;
		rendering::Shader::FloatUniform diffuseTexAlpha;
		rendering::Shader::BoolUniform diffuseTexEnabled;

		bool useDiffuseTex{false};
		float textureAlpha{0};

		rendering::Textures textures{2};

		CubeMaterial() {
			name = "DefaultCubeMaterial";

			textures.defaultOptions.repeat = nage::rendering::Textures::Options::R_Clamp;
			textures.defaultOptions.filter = nage::rendering::Textures::Options::F_Trilinear;
			textures.defaultOptions.anisotropyFilter = 8;

			utils::Image image;
			image.open("assets/images/box_dm.jpg");
			textures.loadFromSurface(0, image.getSurface());

			textures.createFromColor(1, 512, 512, [](GLsizei w, GLsizei h, GLsizei x, GLsizei y) -> SDL_Color {
				auto r = Uint8(float(x) / float(w) * 255);
				auto g = Uint8(float(y) / float(h) * 255);
				return SDL_Color{r, g, 255, 255};
			});
		}

		void setup(rendering::Shader &shader) {
			projectionMatrix = shader.getProjectionMatrix();
			viewMatrix = shader.getViewMatrix();
			modelMatrix = shader.getModelMatrix();

			pos = shader.getAttrib("attrPosition");
			tex = shader.getAttrib("attrTexcoord");
			norm = shader.getAttrib("attrNormal");

			shader.setTypedUniform("uDiffuseTex1", diffuseTex1);
			shader.setTypedUniform("uDiffuseTex2", diffuseTex2);
			shader.setTypedUniform("uDiffuseTexEnabled", diffuseTexEnabled);
			shader.setTypedUniform("uDiffuseTexAlpha", diffuseTexAlpha);
		}

		void installAttribs() const {
			pos.trySetVertexAttribPointer(3, GL_FLOAT, GL_FALSE, sizeof(rendering::Vertex),
										  (GLvoid *) offsetof(rendering::Vertex, position));
			tex.trySetVertexAttribPointer(2, GL_FLOAT, GL_FALSE, sizeof(rendering::Vertex),
										  (GLvoid *) offsetof(rendering::Vertex, texcoord));
			norm.trySetVertexAttribPointer(3, GL_FLOAT, GL_FALSE, sizeof(rendering::Vertex),
										   (GLvoid *) offsetof(rendering::Vertex, normal));
		}


		void updateUniforms() const override {
			diffuseTexEnabled = useDiffuseTex;
			diffuseTex1 = 0;
			diffuseTex2 = 1;
			diffuseTexAlpha = abs(textureAlpha / 5);
		}

		void updateModelMatrix(const glm::mat4 &matrix) const override {
			modelMatrix = matrix;
		}

		void updateViewMatrix(const glm::mat4 &matrix) const override {
			viewMatrix = matrix;
		}

		void updateProjectionMatrix(const glm::mat4 &matrix) const override {
			projectionMatrix = matrix;
		}

		void bindTextures() const override {
			textures.bind(0, 0);
			textures.bind(1, 1);
		}

		void unbindTextures() const override {
			rendering::Textures::unbind(0);
			rendering::Textures::unbind(1);
		}

		void enableAttribs() const override {
			pos.tryEnableVertexAttribArray();
			tex.tryEnableVertexAttribArray();
			norm.tryEnableVertexAttribArray();
		}

		void disableAttribs() const override {
			pos.tryDisableVertexAttribArray();
			tex.tryDisableVertexAttribArray();
			norm.tryDisableVertexAttribArray();
		}
	};

	std::unique_ptr<CubeMaterial> material;

	scene::Model model;

	CubeModel() {
		material = std::make_unique<CubeMaterial>();

		model.materials[1] = material.get();

		rendering::Mesh::Vertices cubeVertices{
				// bottom
				{{-1, -1, 1},  {0, 0}, {0,  -1, 0}},
				{{-1, -1, -1}, {0, 1}, {0,  -1, 0}},
				{{1,  -1, -1}, {1, 1}, {0,  -1, 0}},
				{{1,  -1, 1},  {1, 0}, {0,  -1, 0}},
				// back
				{{1,  1,  -1}, {0, 0}, {0,  0,  -1}},
				{{1,  -1, -1}, {0, 1}, {0,  0,  -1}},
				{{-1, -1, -1}, {1, 1}, {0,  0,  -1}},
				{{-1, 1,  -1}, {1, 0}, {0,  0,  -1}},
				// left
				{{-1, 1,  -1}, {0, 0}, {-1, 0,  0}},
				{{-1, -1, -1}, {0, 1}, {-1, 0,  0}},
				{{-1, -1, 1},  {1, 1}, {-1, 0,  0}},
				{{-1, 1,  1},  {1, 0}, {-1, 0,  0}},
				// front
				{{-1, 1,  1},  {0, 0}, {0,  0,  1}},
				{{-1, -1, 1},  {0, 1}, {0,  0,  1}},
				{{1,  -1, 1},  {1, 1}, {0,  0,  1}},
				{{1,  1,  1},  {1, 0}, {0,  0,  1}},
				// right
				{{1,  1,  1},  {0, 0}, {1,  0,  0}},
				{{1,  -1, 1},  {0, 1}, {1,  0,  0}},
				{{1,  -1, -1}, {1, 1}, {1,  0,  0}},
				{{1,  1,  -1}, {1, 0}, {1,  0,  0}},
				// top
				{{-1, 1,  -1}, {0, 0}, {0,  1,  0}},
				{{-1, 1,  1},  {0, 1}, {0,  1,  0}},
				{{1,  1,  1},  {1, 1}, {0,  1,  0}},
				{{1,  1,  -1}, {1, 0}, {0,  1,  0}},
		};
		rendering::Mesh::Indices cubeIndices{
				0, 1, 2, 0, 2, 3,
				4, 5, 6, 4, 6, 7,
				8, 9, 10, 8, 10, 11,
				12, 13, 14, 12, 14, 15,
				16, 17, 18, 16, 18, 19,
				20, 21, 22, 20, 22, 23
		};

		model.appendMesh(1, rendering::Mesh(cubeVertices, cubeIndices));
	}

	void setup(rendering::Shader &shader) {
		material->setup(shader);

		for (const auto &it: model.meshes) {
			for (const auto &mesh: it.second) {
				mesh.bind();
				material->installAttribs();
				rendering::Mesh::unbind();
			}
		}
	}

};

using InterpolateFunc = float (*)(float);

static float interpolateLinear(float t) {
	return t;
}

static float interpolateAccelerateDecelerate(float t) {
	return float(cos((t + 1) * M_PI)) / 2 + 0.5f;
}

static float interpolateOvershoot(float t, float tension) {
	t -= 1.0f;
	return t * t * ((tension + 1) * t + tension) + 1.0f;
}

static float interpolateOvershoot2(float t) {
	return interpolateOvershoot(t, 2);
}

static float interpolateAnticipate(float t, float tension) {
	return t * t * ((tension + 1) * t - tension);
}

static float interpolateAnticipate2(float t) {
	return interpolateAnticipate(t, 2);
}

static float _interpolateBounceImpl(float t) {
	return t * t * 8.0f;
}

static float interpolateBounce(float t) {
	t *= 1.1226f;
	if (t < 0.3535f) return _interpolateBounceImpl(t);
	else if (t < 0.7408f) return _interpolateBounceImpl(t - 0.54719f) + 0.7f;
	else if (t < 0.9644f) return _interpolateBounceImpl(t - 0.8526f) + 0.9f;
	else return _interpolateBounceImpl(t - 1.0435f) + 0.95f;
}

struct CubeObject : public scene::Object {
	scene::Camera *camera{nullptr};

	rendering::Shader shader;
	CubeModel cube;
	bool wireframe{false};
	float scaleX{0};
	InterpolateFunc interpolateFunc{interpolateLinear};

	CubeObject() {
		shader.attachFile("assets/shaders/simple_vertex.glsl", rendering::ShaderSource::Vertex);
		shader.attachFile("assets/shaders/simple_frag.glsl", rendering::ShaderSource::Fragment);
		shader.link();

		cube.model.rotation.x = glm::radians(15.0f);
		model = &cube.model;
		modelShader = &shader;

		shader.use();
		cube.setup(shader);
		rendering::Shader::unuse();
	}

	bool onFilterEvent(const SDL_Event &event) override {
		if (event.type == SDL_KEYDOWN && event.key.repeat == 0) {
			if (event.key.keysym.scancode == SDL_SCANCODE_F11) {
				auto window = app::Application::getWindow();
				if (window->getFullscreenMode() != app::WindowFullscreenMode::Windowed) {
					window->setFullscreen(app::WindowFullscreenMode::Windowed);
				} else {
					window->setFullscreen(app::WindowFullscreenMode::FullBorderless);
				}
			}
			if (event.key.keysym.scancode == SDL_SCANCODE_F10) {
				app::Application::quit();
			}
			if (event.key.keysym.scancode == SDL_SCANCODE_TAB) {
				wireframe = !wireframe;
				if (wireframe)
					glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				else
					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			}
			if (event.key.keysym.scancode == SDL_SCANCODE_T) {
				cube.material->useDiffuseTex = !cube.material->useDiffuseTex;
			}
			if (event.key.keysym.scancode == SDL_SCANCODE_1) {
				interpolateFunc = interpolateLinear;
			}
			if (event.key.keysym.scancode == SDL_SCANCODE_2) {
				interpolateFunc = interpolateAccelerateDecelerate;
			}
			if (event.key.keysym.scancode == SDL_SCANCODE_3) {
				interpolateFunc = interpolateOvershoot2;
			}
			if (event.key.keysym.scancode == SDL_SCANCODE_4) {
				interpolateFunc = interpolateAnticipate2;
			}
			if (event.key.keysym.scancode == SDL_SCANCODE_5) {
				interpolateFunc = interpolateBounce;
			}
			if (event.key.keysym.scancode == SDL_SCANCODE_F9) {
				if (camera->getProjectionMode() == scene::Camera::PM_Orthographic) {
					camera->nearPlane = 0.1f;
					camera->perspective();
				} else {
					camera->nearPlane = -1000;
					camera->orthographic();
				}
			}
		}
		if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
			camera->setViewportSize(event.window.data1, event.window.data2);
		}
		return Object::onFilterEvent(event);
	}

	[[nodiscard]] float calculateScaleX() const {
		auto normalized = (scaleX + 1) / 2;
		auto interpolated = interpolateFunc(normalized);
		return interpolated * 2 - 1;
	}

	void onUpdate(float deltaTime) override {
		scaleX += deltaTime;
		if (scaleX > 1)
			scaleX = -1;
		cube.model.scale.x = abs(calculateScaleX());

		cube.model.rotation.z += deltaTime;
		cube.model.rotation.y += float(Input::gamepadAnyButtonsIsPressed(GamepadButton::DPAD_RIGHT)) * deltaTime * 5;
		cube.model.rotation.y -= float(Input::gamepadAnyButtonsIsPressed(GamepadButton::DPAD_LEFT)) * deltaTime * 5;
		cube.model.rotation.x += float(Input::gamepadAnyButtonsIsPressed(GamepadButton::DPAD_UP)) * deltaTime * 5;
		cube.model.rotation.x -= float(Input::gamepadAnyButtonsIsPressed(GamepadButton::DPAD_DOWN)) * deltaTime * 5;
		cube.model.position.y -= Input::getAnyAxis(Axis::LeftY) * deltaTime * 5;
		cube.model.position.x += Input::getAnyAxis(Axis::LeftX) * deltaTime * 5;
		cube.model.applyTransformation();

		cube.material->textureAlpha += deltaTime;
		if (cube.material->textureAlpha > 5)
			cube.material->textureAlpha = -5;

		camera->position.x -= float(Input::gamepadAnyButtonsIsPressed(GamepadButton::X)) * deltaTime;
		camera->position.x += float(Input::gamepadAnyButtonsIsPressed(GamepadButton::B)) * deltaTime;
		camera->position.y -= float(Input::gamepadAnyButtonsIsPressed(GamepadButton::A)) * deltaTime;
		camera->position.y += float(Input::gamepadAnyButtonsIsPressed(GamepadButton::Y)) * deltaTime;
		camera->position.z += Input::getAnyAxis(Axis::TriggerLeft) * deltaTime;
		camera->position.z -= Input::getAnyAxis(Axis::TriggerRight) * deltaTime;
		camera->rotation.pitch -= Input::getAnyAxis(Axis::RightY) * deltaTime;
		camera->rotation.yaw += Input::getAnyAxis(Axis::RightX) * deltaTime;

		camera->lookAt();
	}

};

struct MainScene : public scene::Scene {
	scene::Camera camera;
	CubeObject cubeObject;

	MainScene() {
		camera.orthographicSize = 6;
		camera.nearPlane = -1000;
		camera.position.z = 2;
		camera.rotation.yaw = -M_PI / 2;

		camera.lookAt();
		camera.orthographic();

		cubeObject.camera = &camera;

		cameras.push_back(&camera);
		nodes.push_back(&cubeObject);

		activateAll();
	}

};

int main(int argc, char *argv[]) {
	try {
		std::cout << "powered by '" NAGE_NAME_FULL "' v" NAGE_VERSION_STRING << std::endl;

		if (!app::Instance::lockSingleInstance("MySuperGame")) {
			std::cout << "game is already started" << std::endl;
			return 0;
		}

		app::Options appOptions;
		std::list<std::string> args;
		appOptions.parse(argc, argv, app::Options::PM_InvalidArgToStdErr, &args);
		appOptions.title = "TestGame";

		app::Application app(appOptions);

		for (const auto &arg: args) {
			if (arg == "-vsync") {
				app::Application::setSwapInterval(app::Application::SI_VSync);
			} else if (arg == "-adaptive") {
				app::Application::setSwapInterval(app::Application::SI_Adaptive);
			} else if (arg == "-immediately") {
				app::Application::setSwapInterval(app::Application::SI_Immediately);
			} else if (arg == "-msaa") {
				app::Application::setAliasMode(nage::app::Application::AM_Multisample);
			}
		}

		Input::bindAxis(Axis::LeftX)
				.addKey(SDL_SCANCODE_A, -1)
				.addKey(SDL_SCANCODE_D, 1);
		Input::bindAxis(Axis::LeftY)
				.addKey(SDL_SCANCODE_W, -1)
				.addKey(SDL_SCANCODE_S, 1);
		Input::bindAxis(Axis::RightX)
				.addKey(SDL_SCANCODE_KP_9, 1)
				.addKey(SDL_SCANCODE_KP_7, -1);
		Input::bindAxis(Axis::RightY)
				.addKey(SDL_SCANCODE_KP_MULTIPLY, -1)
				.addKey(SDL_SCANCODE_KP_DIVIDE, 1);
		Input::bindAxis(Axis::TriggerRight)
				.addKey(SDL_SCANCODE_KP_PLUS, 1);
		Input::bindAxis(Axis::TriggerLeft)
				.addKey(SDL_SCANCODE_KP_MINUS, 1);
		Input::bindButton(GamepadButton::X)
				.addKey(SDL_SCANCODE_KP_4);
		Input::bindButton(GamepadButton::B)
				.addKey(SDL_SCANCODE_KP_6);
		Input::bindButton(GamepadButton::Y)
				.addKey(SDL_SCANCODE_KP_8);
		Input::bindButton(GamepadButton::A)
				.addKey(SDL_SCANCODE_KP_2);
		Input::bindButton(GamepadButton::DPAD_LEFT)
				.addKey(SDL_SCANCODE_LEFT);
		Input::bindButton(GamepadButton::DPAD_RIGHT)
				.addKey(SDL_SCANCODE_RIGHT);
		Input::bindButton(GamepadButton::DPAD_UP)
				.addKey(SDL_SCANCODE_UP);
		Input::bindButton(GamepadButton::DPAD_DOWN)
				.addKey(SDL_SCANCODE_DOWN);

		InputEventListenerImpl iel;
		Input::installEventListener(&iel);

		Input::setGamepadDeadZone(3000);

		MainScene m;
		app::Application::setScene(&m);

		auto w = app::Application::getWindow();
		m.camera.setViewportSize(w->getWidth(), w->getHeight());

		return app.start();
	} catch (const std::exception &err) {
		std::cout << "error: " << err.what() << std::endl;
	} catch (...) {
		std::cout << "unhandled error" << std::endl;
	}
	return 1;
}
