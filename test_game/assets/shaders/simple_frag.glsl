#version 400

in vec4 vs_position;
in vec2 vs_texcoord;
in vec3 vs_normal;

out vec4 fs_color;

uniform sampler2D uDiffuseTex1;
uniform sampler2D uDiffuseTex2;
uniform bool uDiffuseTexEnabled;
uniform float uDiffuseTexAlpha;

void main() {
    if (!uDiffuseTexEnabled) {
        fs_color.rgb = (vs_position.xyz + 1) / 2 * vs_normal;
    } else {
        vec4 a = texture2D(uDiffuseTex1, vs_texcoord) * (1 - uDiffuseTexAlpha);
        vec4 b = texture2D(uDiffuseTex2, vs_texcoord) * uDiffuseTexAlpha;
        fs_color = a + b;
    }
    fs_color.a = 1;
}
