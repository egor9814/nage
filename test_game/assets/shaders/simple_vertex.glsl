#version 400

layout (location = 0) in vec3 attrPosition;
layout (location = 1) in vec2 attrTexcoord;
layout (location = 2) in vec3 attrNormal;

out vec4 vs_position;
out vec2 vs_texcoord;
out vec3 vs_normal;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;

void main() {
    vs_position = uModelMatrix * vec4(attrPosition.xyz, 1);
    vs_texcoord = attrTexcoord;
    vs_normal = mat3(uModelMatrix) * attrNormal;

    gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(attrPosition.xyz, 1);
}
