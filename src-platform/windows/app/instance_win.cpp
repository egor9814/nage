//
// Copyright 2022 Chalyh Egor
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <nage/app/instance.hpp>
#include "windows.h"

namespace nage::app {

	struct WindowsHandle : public Instance::IHandle {
		HANDLE mutex{nullptr};

		bool open(const char *id) override {
			if (mutex)
				return false;
			auto mtx = OpenMutex(MUTEX_ALL_ACCESS, 0, id);
			if (!mtx) {
				mutex = CreateMutex(nullptr, 0, id);
				return mutex != nullptr;
			}
			return false;
		}

		void close() override {
			if (mutex) {
				ReleaseMutex(mutex);
				mutex = nullptr;
			}
		}
	};

	void Instance::resetHandle() {
		if (!m_handle)
			m_handle = std::make_unique<WindowsHandle>();
	}

}